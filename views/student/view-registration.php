<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Student */

$this->title = "{$model->firstName} {$model->middleName} {$model->lastName}";
//$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kurs list'), 'url' => ['/kurs/public']];
$this->params['breadcrumbs'][] = ['label' => $kurs->name_kurs, 'url' => ['/kurs/details','id'=>$kurs->id_kurs]];
$this->params['breadcrumbs'][] = $group->name_group;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-view">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a(Yii::$app->lang->t('Update'), ['update-registration', 'id_stud' => $model->id_stud, 'secret_code'=>$model->secret_code ], ['class' => 'btn btn-primary btn-xs']) ?>
        <?= Html::a(Yii::$app->lang->t('Delete'), ['delete-registration', 'id_stud' => $model->id_stud, 'secret_code'=>$model->secret_code ], [
            'class' => 'btn btn-danger btn-xs',
            'data' => [
                'confirm' => Yii::$app->lang->t('Confirm removal'),
                'method' => 'post',
            ],
        ]) ?>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
         //   'id_stud',
         [
            'label' => Yii::$app->lang->t('Course'),
            'value' => $kurs->name_kurs,
         ],
         [
            'label' => Yii::$app->lang->t('Group'),
            'value' => $group->name_group,
         ], 
         [
            'label' => Yii::$app->lang->t('Deadline'),
            'value' => date(Yii::$app->params['dateFormat'], strtotime($group->date_start)),
         ],   
            'firstName',
            'middleName',
            'lastName',

            'email:email',
          //  'secret_code',
          //   'anketa',
        ],
    ]) ?>
    
    <?php
    $obj = new \app\lib\ShowAnketa('anketa');
    $post=['anketa'=> array_map(function($x){return $x['value'];},  json_decode($model->anketa,true))];

    $anketa = $group->blankAnkety();
    //return json_encode($anketa);
    echo $obj->view($anketa, $post);
    ?> 

</div>
