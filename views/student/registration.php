<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;



\kartik\select2\Select2Asset::register($this);


/* @var $this yii\web\View */
/* @var $group app\groups\Group */
$kurs = $group->idKurs;
if (Yii::$app->user->isGuest) {
    $this->params['breadcrumbs'][] = ['label' =>  ($kurs ? $kurs->name_kurs : ''), 'url' => ['/kurs/details', 'id' => $group->id_kurs]];
} else {
    $this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kurs list'), 'url' => ['/kurs/index']];
    $this->params['breadcrumbs'][] = ['label' => ($kurs ? $kurs->name_kurs : ''), 'url' => ['/kurs/view', 'id' => $group->id_kurs]];
    $this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Groups'), 'url' => ['/group/index', 'id_kurs' => $group->id_kurs]];
    $this->params['breadcrumbs'][] = ['label' => $group->name_group, 'url' => ['/group/view', 'id' => $group->id_group]];
}
$this->title = Yii::$app->lang->t('Registration');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <h2><?= $kurs->name_kurs ?></h2>
    <p><?= $kurs->description ?></p>
    <p><?= $group->name_group ?></p>
    <p><?=Yii::$app->lang->t('Deadline')?> <?= date(Yii::$app->params['dateFormat'], strtotime($group->date_start)) ?></p>
    <p><?=Yii::$app->lang->t('NumOfMembers')?> <?=$group->getStudents()->count()?> / <?=$group->max_count_stud?></p>

    
    
    <?php
    if($postedAnketaData){
        foreach($postedAnketaData as $pad){
            foreach($pad['errors'] as $err){
                echo "<div class='alert alert-danger'>{$err}</div>";
            }
        }
    }
    ?>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id'=>'studentUpdateForm']]); ?> 
    <?= $form->field($student, 'lastName')->textInput(['maxlength' => true])->label(Yii::$app->lang->t('lastName')."*") ?>
    <?= $form->field($student, 'firstName')->textInput(['maxlength' => true])->label(Yii::$app->lang->t('firstName')."*") ?>
    <?= $form->field($student, 'middleName')->textInput(['maxlength' => true]) ?>


    <?= $form->field($student, 'email')->textInput(['maxlength' => true])->label(Yii::$app->lang->t('email')."*") ?>

    <?php
    $obj = new \app\lib\ShowAnketa('anketa');

    $anketa = $group->blankAnkety();
    //return json_encode($anketa);
    echo $obj->form($anketa, $post);
    ?>
    <br>
    <br>
    <input type='submit' value='<?=Yii::$app->lang->t('Signup')?>' class='btn btn-primary'>

    <?php ActiveForm::end(); ?> 
</div>

<?php $this->registerCssFile(Yii::getAlias('@web/css/bootstrap-datepicker.min.css')); ?>
<?php $this->registerJSFile(Yii::getAlias('@web/js/bootstrap-datepicker.min.js'),['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJSFile(Yii::getAlias('@web/js/bootstrap-datepicker.uk.min.js'),['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJSFile(Yii::getAlias('@web/js/showanketa.js'),['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php
   $this->registerJs("
       window.alertShown=false;
       window.enableAlertDelayed=function(){window.alertShown=false;};
       $('#studentUpdateForm').submit(function(event){
          if(!anketa.fn.validate()){
            if(!window.alertShown){
               alert( '".Yii::$app->lang->t('FillInCorrectData')."' );
               window.alertShown=true;
               setTimeout(window.enableAlertDelayed, 1000);
            }
            event.preventDefault();
          }
       });
   ");
?>
