<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;
use app\models\Student;

/* @var $this yii\web\View */
/* @var $student app\models\Student */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id'=>'studentUpdateForm']]); ?>


    <?php
    echo $form->field($student, 'id_group')->widget(Select2::classname(), [
        'data' => $groupOptions,
        'options' => ['placeholder' => Yii::$app->lang->t('Select group')],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => false
        ],
    ])->label(Yii::$app->lang->t('Group'));
    ?>
    <?= $form->field($student, 'lastName')->textInput(['maxlength' => true])->label(Yii::$app->lang->t('lastName').'*') ?>
    <?= $form->field($student, 'firstName')->textInput(['maxlength' => true])->label(Yii::$app->lang->t('firstName').'*') ?>
    <?= $form->field($student, 'middleName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($student, 'email')->textInput(['maxlength' => true])->label(Yii::$app->lang->t('email').'*') ?>
    
    <?= $form->field($student, 'status')->dropDownList(Student::getStatusOptions(), [])?>

    <?php /* = $form->field($student, 'secret_code')->textInput(['maxlength' => true]) */ ?>

    <?php /* = $form->field($student, 'anketa')->textInput() */ ?>

    <?php
    // draw anketa
    
    $blank = $group->blankAnkety();
    
    $obj = new \app\lib\ShowAnketa('anketa');

    

    if ($anketaData) {
        $post = ['anketa' => array_map(function($x) { return $x['value'];  }, $anketaData)];
    } else {
        $post = ['anketa' => []];
    }
    echo $obj->form($blank, $post);
    ?>

    <div class="form-group">
<?= Html::submitButton($student->isNewRecord ? Yii::$app->lang->t('Create') : Yii::$app->lang->t('Update'), ['class' => $student->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>

<?php $this->registerCssFile(Yii::getAlias('@web/css/bootstrap-datepicker.min.css')); ?>
<?php $this->registerJSFile(Yii::getAlias('@web/js/bootstrap-datepicker.min.js'),['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJSFile(Yii::getAlias('@web/js/bootstrap-datepicker.ru.min.js'),['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJSFile(Yii::getAlias('@web/js/showanketa.js'),['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php
   $this->registerJs("
       window.alertShown=false;
       window.enableAlertDelayed=function(){window.alertShown=false;};
       $('#studentUpdateForm').submit(function(event){
          if(!anketa.fn.validate()){
            if(!window.alertShown){
               alert( '".Yii::$app->lang->t('FillInCorrectData')."' );
               window.alertShown=true;
               setTimeout(window.enableAlertDelayed, 1000);
            }
            event.preventDefault();
          }
       });
   ");
?>