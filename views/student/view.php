<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Student */

$group = $model->idGroup;
$kurs = $group->idKurs;


$this->title = "{$model->firstName} {$model->middleName} {$model->lastName}";


    

$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kurs list'), 'url' => ['/kurs/index']];
$this->params['breadcrumbs'][] = ['label' => $kurs->name_kurs, 'url' => ['/kurs/view', 'id' => $kurs->id_kurs]];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Groups'), 'url' => ['/group/index', 'id_kurs' => $kurs->id_kurs]];
$this->params['breadcrumbs'][] = ['label' => $group->name_group, 'url' => ['/group/view', 'id' => $group->id_group]];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Members'), 'url' => ['index','id_group' => $group->id_group]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-view">

    <h1><?= Html::encode($this->title) ?>
        <?= Html::a(Yii::$app->lang->t('Update'), ['update', 'id' => $model->id_stud], ['class' => 'btn btn-primary btn-xs']) ?>
        <?= Html::a(Yii::$app->lang->t('Delete'), ['delete', 'id' => $model->id_stud], [
            'class' => 'btn btn-danger btn-xs',
            'data' => [
                'confirm' => Yii::$app->lang->t('Confirm removal'),
                'method' => 'post',
            ],
        ]) ?>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_stud',
         [                      // the owner name of the model
            'label' => Yii::$app->lang->t('Course'),
            'value' => $kurs->name_kurs,
         ],
         [                      // the owner name of the model
            'label' => Yii::$app->lang->t('Group'),
            'value' => $group->name_group,
         ], 
         [                      // the owner name of the model
            'label' => Yii::$app->lang->t('Deadline'),
            'value' => date(Yii::$app->params['dateFormat'], strtotime($group->date_start)),
         ],   
            'firstName',
            'middleName',
            'lastName',
            'email:email',
            'status'
          //  'secret_code',
         //   'anketa',
        ],
    ]) ?>
    
    <?php
    $obj = new \app\lib\ShowAnketa('anketa');

    $anketaData = json_decode($model->anketa,true);

    if($anketaData){
        $post=['anketa'=> array_map(function($x){return $x['value'];}, $anketaData )];
    }else{
        $post=['anketa'=> []];
    }
    // var_dump($anketaData);
    

    $anketa = $group->blankAnkety();
    //return json_encode($anketa);
    
    ?><div class="stud-anketa-view"><?php
    echo $obj->view($anketa, $post);
    ?></div>

</div>
