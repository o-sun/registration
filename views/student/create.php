<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Student */


$this->title = Yii::$app->lang->t('Create');
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kursy'), 'url' => ['/kurs/index']];
$this->params['breadcrumbs'][] = ['label' => $kurs->name_kurs, 'url' => ['/kurs/view', 'id' => $kurs->id_kurs]];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Groups'), 'url' => ['/group/index', 'id_kurs' => $kurs->id_kurs]];
$this->params['breadcrumbs'][] = ['label' => $group->name_group, 'url' => ['/group/view', 'id' => $group->id_group]];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Members'), 'url' => ['index', 'id_group' => $group->id_group]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'model' => $model,
        'groupOptions' => $groupOptions,
        'group' => $group,
        'kurs' => $kurs
    ])
    ?>

</div>
