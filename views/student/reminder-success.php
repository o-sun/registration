<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $student app\models\Student */

$this->title = Yii::$app->lang->t('Reminder');
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kurs list'), 'url' => ['/kurs/public']];
$this->params['breadcrumbs'][] = ['label' => $kurs->name_kurs, 'url' => ['/kurs/details', 'id' => $kurs->id_kurs]];
$this->params['breadcrumbs'][] = Yii::$app->lang->t('Group').' ' . $group->name_group;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-view">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <?php
    if($message){
        echo "<p style=\"color:green;font-size:150%;\">{$message}</p>";
    }
    ?>
    
    <h3><?= $kurs->name_kurs ?></h3>
    <p><?= $kurs->description ?></p>
    <h3><?=Yii::$app->lang->t('Group')?> <?= $group->name_group ?></h3>
    <p><?=Yii::$app->lang->t('Deadline')?> <?= date(Yii::$app->params['dateFormat'], strtotime($group->date_start)) ?></p>
    <p><?=Yii::$app->lang->t('NumOfMembers')?> <?=$group->getStudents()->count()?> / <?=$group->max_count_stud?></p>




</div>
