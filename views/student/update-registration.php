<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $student app\models\Student */

\kartik\select2\Select2Asset::register($this);

$this->title = Yii::$app->lang->t('Update member').' ' . "{$student->firstName} {$student->middleName} {$student->lastName}";
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kurs list'), 'url' => ['/kurs/public']];
$this->params['breadcrumbs'][] = ['label' => $kurs->name_kurs, 'url' => ['/kurs/details', 'id' => $kurs->id_kurs]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
    if($anketaData){
        foreach($anketaData as $pad){
            foreach($pad['errors'] as $err){
                echo "<div class='color:red;'>{$err}</div>";
            }
        }
    }
    ?>
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','id'=>'studentUpdateForm']]); ?>


    <?php /*
    echo $form->field($student, 'id_group')->widget(Select2::classname(), [
        'data' => $groupOptions,
        'options' => ['placeholder' => 'Выберите группу ...'],
        'pluginOptions' => [
            'allowClear' => true,
            'multiple' => false
        ],
    ]);
    */ ?>
    <?= $form->field($student, 'lastName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($student, 'firstName')->textInput(['maxlength' => true]) ?>
    <?= $form->field($student, 'middleName')->textInput(['maxlength' => true]) ?>


    <?php /* $form->field($student, 'email')->textInput(['maxlength' => true]) */ ?>
    <?php /* = $form->field($student, 'secret_code')->textInput(['maxlength' => true]) */ ?>
    <?php /* = $form->field($student, 'anketa')->textInput() */ ?>
    <?php
    // draw anketa
    
    $blank = $group->blankAnkety();
    
    $obj = new \app\lib\ShowAnketa('anketa');

    

    if ($anketaData) {
        $post = ['anketa' => array_map(function($x) { return $x['value'];  }, $anketaData)];
    } else {
        $post = ['anketa' => []];
    }
    echo $obj->form($blank, $post);
    ?>

    <div class="form-group">
     <?= Html::submitButton($student->isNewRecord ? Yii::$app->lang->t('Create') : Yii::$app->lang->t('Update'), ['class' => $student->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>

</div>
<?php $this->registerCssFile(Yii::getAlias('@web/css/bootstrap-datepicker.min.css')); ?>
<?php $this->registerJSFile(Yii::getAlias('@web/js/bootstrap-datepicker.min.js'),['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJSFile(Yii::getAlias('@web/js/bootstrap-datepicker.uk.min.js'),['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJSFile(Yii::getAlias('@web/js/showanketa.js'),['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php
   $this->registerJs("
       window.alertShown=false;
       window.enableAlertDelayed=function(){window.alertShown=false;};
       $('#studentUpdateForm').submit(function(event){
          if(!anketa.fn.validate()){
            if(!window.alertShown){
               alert( '".Yii::$app->lang->t('FillInCorrectData')."' );
               window.alertShown=true;
               setTimeout(window.enableAlertDelayed, 1000);
            }
            event.preventDefault();
          }
       });
   ");
?>