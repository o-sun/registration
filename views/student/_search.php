<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\StudentSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="student-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_stud') ?>

    <?= $form->field($model, 'id_group') ?>

    <?= $form->field($model, 'Fio') ?>

    <?= $form->field($model, 'email') ?>

    <?= $form->field($model, 'secret_code') ?>

    <?php // echo $form->field($model, 'anketa') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::$app->lang->t('Find'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::$app->lang->t('Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
