<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

use app\models\Student;
/* @var $this yii\web\View */
/* @var $searchModel app\models\StudentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->lang->t('Members');
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kursy'), 'url' => ['/kurs/index']];
$this->params['breadcrumbs'][] = ['label' => ($kurs?$kurs->name_kurs:''), 'url' => ['/kurs/view', 'id'=>$group->id_kurs]];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Groups'), 'url' => ['/group/index', 'id_kurs'=>$group->id_kurs]];
$this->params['breadcrumbs'][] = ['label' => $group->name_group, 'url' => ['/group/view', 'id' => $group->id_group]];
$this->params['breadcrumbs'][] = $this->title;



$statusOptions=Student::getStatusOptions();

?>
<div class="student-index">
    <h1><?= Html::encode($this->title) ?> 
        <?= Html::a(Yii::$app->lang->t('Add member'), ['create','id_group'=>$group->id_group], ['class' => 'btn btn-success btn-xs']) ?>
    </h1>
    
    
    <div class="student-search">

    <span class="student-search-form">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <span class="student-search-elm"><?= $form->field($searchModel, 'status')->dropDownList($statusOptions, []) ?></span>
    <span class="student-search-elm"><?= $form->field($searchModel, 'keyWords') ?></span>
    <span class="student-search-elm"><?= $form->field($searchModel, 'dateCreated') ?></span>
    <span class="student-search-elm" style="width:auto;">
        <label for="studentsearch-datecreated" class="control-label">&nbsp;</label><br>
        <?= Html::submitButton(Yii::$app->lang->t('Find'), ['class' => 'btn btn-primary']) ?>
    </span>
    <input type="hidden" name="id_group" value="<?=htmlspecialchars($group->id_group)?>">
    <?php ActiveForm::end(); ?>
    </span>
    <span class="student-search-form">
    <?php $form = ActiveForm::begin([
        'action' => ['download'],
        'method' => 'get',
    ]); ?>

    <span class="student-search-elm" style="width:auto;">
        <label for="studentsearch-datecreated" class="control-label">&nbsp;</label><br>
        <?= Html::submitButton(Yii::$app->lang->t('Download members'), ['class' => 'btn btn-primary']) ?>
    </span>
            <input type="hidden" name="id_group" value="<?=htmlspecialchars($group->id_group)?>">
            <input type="hidden" name="StudentSearch[keyWords]" value="<?=htmlspecialchars($searchModel->keyWords)?>">
            <input type="hidden" name="StudentSearch[dateCreated]" value="<?=htmlspecialchars($searchModel->dateCreated)?>">
    <?php ActiveForm::end(); ?>
    </span>

</div>
    
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\ActionColumn'],
            [
                 'attribute'=>'id_stud',
                 'label'=>Yii::$app->lang->t('Code'),
                 'filterOptions' => ['class' => 'filterId'],
            ],
            // 'idGroup.name_group',
            // 'idGroup.idKurs.name_kurs',
            'firstName',
            'middleName',
            'lastName',
            'email:email',
            [
                 'attribute'=>'status',
                 'label'=>Yii::$app->lang->t('status'),
                 'filterOptions' => ['class' => 'filterId'],
                 'content'=>function($model, $key, $index, $column) use($statusOptions){
                        return Html::dropDownList(
                                "status[".$model->id_stud."]",
                                $model->status,
                                $statusOptions,[
                                    'class'=>'form-control status-selector',
                                    'data-id-stud' => $model->id_stud
                                ]);
                 }
            ],
            [
                 'attribute'=>'dateCreated',
                 'label'=>Yii::$app->lang->t('dateCreated'),
                 'filterOptions' => ['class' => 'filterId'],
             ],
            // 'secret_code',
            // 'anketa',
        ],
    ]); ?>
<?php $this->registerCssFile(Yii::getAlias('@web/css/bootstrap-datepicker.min.css')); ?>
<?php $this->registerJSFile(Yii::getAlias('@web/js/bootstrap-datepicker.min.js'),['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php $this->registerJSFile(Yii::getAlias('@web/js/bootstrap-datepicker.uk.min.js'),['depends' => [\yii\web\JqueryAsset::className()]]); ?>
<?php
   $this->registerJs("
       \$('#studentsearch-datecreated').datepicker({
          format: 'yyyy-mm-dd',
          language:'uk'
       });
       \$('.status-selector').change(function(event){
            var elm=\$(event.currentTarget);
            var newValue = elm.val();
            var idStudent = elm.attr('data-id-stud');
            \$.ajax({
                url: \"index.php?r=student%2Fsetstatus\",
                data:{id_stud:idStudent, status:newValue},
                method:'post',
                context: document.body
            }).done(function() {
                // $( this ).addClass( \"done\" );
            });
       });
   ");
?>
</div>
