<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $student app\models\Student */



$this->title = Yii::$app->lang->t('Update member').' ' . "{$student->firstName} {$student->middleName} {$student->lastName}";
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kurs list'), 'url' => ['/kurs/index']];
$this->params['breadcrumbs'][] = ['label' => $kurs->name_kurs, 'url' => ['/kurs/view', 'id' => $kurs->id_kurs]];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Groups'), 'url' => ['/group/index', 'id_kurs' => $kurs->id_kurs]];
$this->params['breadcrumbs'][] = ['label' => $group->name_group, 'url' => ['/group/view', 'id' => $group->id_group]];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Members'), 'url' => ['index', 'id_group' => $group->id_group]];
$this->params['breadcrumbs'][] = ['label' => "{$student->firstName} {$student->middleName} {$student->lastName}", 'url' => ['view', 'id' => $student->id_stud]];
$this->params['breadcrumbs'][] = Yii::$app->lang->t('Update');
?>
<div class="student-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?=
    $this->render('_form', [
        'student' => $student,
        'groupOptions' => $groupOptions,
        'group' => $group,
        'kurs' => $kurs,
        'anketaData'=>$anketaData
    ])
    ?>

</div>
