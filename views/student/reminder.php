<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $student app\models\Student */

$this->title = Yii::$app->lang->t('Reminder');
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kurs list'), 'url' => ['/kurs/public']];
$this->params['breadcrumbs'][] = ['label' => $kurs->name_kurs, 'url' => ['/kurs/details', 'id' => $kurs->id_kurs]];
$this->params['breadcrumbs'][] = Yii::$app->lang->t('Group').' ' . $group->name_group;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-view">

    <h1><?= Html::encode($this->title) ?></h1>

    
    <?php
    if($message){
        echo "<p style=\"color:red;font-size:150%;\">{$message}</p>";
    }
    ?>
    
    <h3><?=Yii::$app->lang->t('Kurs')?> <?= $kurs->name_kurs ?></h3>
    <p><?= $kurs->description ?></p>
    <h3><?= $group->name_group ?></h3>
    <p><?=Yii::$app->lang->t('Deadline')?> <?= date(Yii::$app->params['dateFormat'], strtotime($group->date_start)) ?></p>
    <p><?=Yii::$app->lang->t('NumOfMembers')?> <?=$group->getStudents()->count()?> / <?=$group->max_count_stud?></p>

    <div class="form-group">
    <h3><?=Yii::$app->lang->t('SecureLinkHint',['date'=>date(Yii::$app->params['dateFormat'], strtotime($group->date_start))])?></h3>

    <?php $form = ActiveForm::begin(); ?>

    <div class="form-group field-kurs-name_kurs required has-success">
    <label class="control-label" for="kurs-name_kurs"><?=Yii::$app->lang->t('Your email')?></label>
    <input id="kurs-name_kurs" class="form-control" type="text" maxlength="255" value="" name="email">
    <div class="help-block"></div>
    </div>
    
    <?= Html::submitButton(Yii::$app->lang->t('GetSecureLink'), ['class' => 'btn btn-primary' ]) ?>
    <?php ActiveForm::end(); ?>
    </div>



</div>
