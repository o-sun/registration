<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $student app\models\Student */

$this->title = Yii::$app->lang->t('RegistrationCancel');
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kursy'), 'url' => ['/kurs/public']];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kurs {name_kurs}',['name_kurs'=>$kurs->name_kurs]), 'url' => ['/kurs/details', 'id' => $kurs->id_kurs]];
$this->params['breadcrumbs'][] = Yii::$app->lang->t('Group {name_group}',['name_group'=>$group->name_group]);
$this->params['breadcrumbs'][] = ['label' => "{$student->firstName} {$student->middleName} {$student->lastName}", 'url' => ['view', 'id' => $student->id_stud]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-view">

    <h1><?= Html::encode($this->title) ?>
    </h1>


    <div class="form-group">
    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <p><?=Yii::$app->lang->t('Confirm removal')?></p>

    <input type='hidden' name='del' value='1'>
    <?= Html::submitButton(Yii::$app->lang->t('RegistrationCancel'), ['class' => 'btn btn-danger' ]) ?>

    <?php ActiveForm::end(); ?>
    </div>

    <?=
    DetailView::widget([
        'model' => $student,
        'attributes' => [
            //   'id_stud',
            [
                'label' => Yii::$app->lang->t('Course'),
                'value' => $kurs->name_kurs,
            ],
            [
                'label' => Yii::$app->lang->t('Group'),
                'value' => $group->name_group,
            ],
            [
                'label' => Yii::$app->lang->t('Deadline'),
                'value' => date(Yii::$app->params['dateFormat'], strtotime($group->date_start)),
            ],
            'Fio',
            'email:email',
        //  'secret_code',
        //   'anketa',
        ],
    ])
    ?>

    <?php
    $obj = new \app\lib\ShowAnketa('anketa');
    $post = ['anketa' => array_map(function($x) {
                    return $x['value'];
                }, json_decode($student->anketa, true))];

    $anketa = $group->blankAnkety();
    //return json_encode($anketa);
    echo $obj->view($anketa, $post);
    ?> 

</div>
