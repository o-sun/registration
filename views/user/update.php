<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User1 */

$this->title =  Yii::$app->lang->t('Update') . " {$model->userLogin} (id={$model->userId})";
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => " {$model->userLogin} (id={$model->userId})", 'url' => ['view', 'id' => $model->userId]];
$this->params['breadcrumbs'][] = Yii::$app->lang->t('Update');
?>
<div class="user1-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'kurses'=>$kurses,
    ]) ?>

</div>