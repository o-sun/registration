<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\User1 */

$this->title = Yii::$app->lang->t('Create');
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('User'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user1-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'kurses'=>$kurses,
    ]) ?>

</div>