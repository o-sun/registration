<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Kurs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <h2><?=Yii::$app->lang->t('Basic data')?></h2>

    <?= $form->field($model, 'userLogin')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'userPIB')->textInput(['maxlength' => true]) ?>
    
    <?= $form->field($model, 'userEmail')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'userContactData')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'userIsActive')->checkbox() ?>

    
    <h2><?=Yii::$app->lang->t('Update password')?></h2>
    <div class="form-group field-user-userpib">
    <label for="user-userpib" class="control-label"><?=Yii::$app->lang->t('Type new password')?></label>
    <input type="text" maxlength="255" name="userPassword1" class="form-control" id="user-userpib">
    <div class="help-block"></div>
    </div>
    <div class="form-group field-user-userpib">
    <label for="user-userpib" class="control-label"><?=Yii::$app->lang->t('Retype new password')?></label>
    <input type="text" maxlength="255" name="userPassword2" class="form-control" id="user-userpib">
    <div class="help-block"></div>
    </div>

    
    <h2><?=Yii::$app->lang->t('Kurs list')?></h2>
    <?php
    if(\app\models\User::isAdmin()){
        echo $form->field($model, 'idKurs')->widget(Select2::classname(), [
            'data' => $kurses,
            'options' => ['placeholder' => Yii::$app->lang->t('Select courses')],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true
            ],
        ]);        
    }
    ?>
    
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::$app->lang->t('Create') : Yii::$app->lang->t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
