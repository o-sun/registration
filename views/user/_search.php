<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user1-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'userId') ?>

    <?= $form->field($model, 'userLogin') ?>

    <?= $form->field($model, 'userPassword') ?>

    <?= $form->field($model, 'userPIB') ?>

    <?= $form->field($model, 'userContactData') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::$app->lang->t('Find'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::$app->lang->t('Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>