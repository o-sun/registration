<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->lang->t('Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user1-index">

    <h1><?= Html::encode($this->title) ?> <?= Html::a(Yii::$app->lang->t('Create'), ['create'], ['class' => 'btn btn-success  btn-xs']) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\ActionColumn'],

            'userId',
            'userLogin',
            // 'userPassword',
            'userPIB',
            // 'userContactData:ntext',
            'userLastLogin',
            'userIsActive',
            // 'userToken',
            // 'userEmail:email',

            
        ],
    ]); ?>

</div>