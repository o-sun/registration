 <?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\User1 */

$this->title = $model->userLogin;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user1-view">

    <h1><?= Html::encode($this->title) ?>


        <?= Html::a(Yii::$app->lang->t('Update'), ['update', 'id' => $model->userId], ['class' => 'btn btn-primary btn-xs']) ?>
        <?= Html::a(Yii::$app->lang->t('Delete'), ['delete', 'id' => $model->userId], [
            'class' => 'btn btn-danger btn-xs',
            'data' => [
                'confirm' => Yii::$app->lang->t('Confirm removal'),
                'method' => 'post',
            ],
        ]) ?>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'userId',
            'userLogin',
            // 'userPassword',
            'userPIB',
            'userContactData:ntext',
            'userLastLogin',
            'userIsActive',
            // 'userToken',
            'userEmail:email',
        ],
    ]) ?>
    
     <div class="manager"> <h4> <?=Yii::$app->lang->t('Kurs list')?> </h4>
       <?php
         $kurses=$model->getKurs()->all();
         
         foreach($kurses as $kurs){
             echo  "<div> {$kurs-> name_kurs} </div>"  ;
         }
         
        ?>         
    </div>    

</div>
