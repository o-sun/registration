<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Kurs */

$this->title = Yii::$app->lang->t('Create course');
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kursy'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kurs-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'userOptions'=>$userOptions,
    ]) ?>

</div>
