<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

\kartik\select2\Select2Asset::register($this);

/* @var $this yii\web\View */
/* @var $model app\models\Kurs */

$this->title = $model->name_kurs;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kurs list'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kurs-view">

    <h1><?= Html::encode($this->title) ?>
    <?= Html::a(Yii::$app->lang->t('Update'), ['update', 'id' => $model->id_kurs], ['class' => 'btn btn-primary btn-xs']) ?>
        <?= Html::a(Yii::$app->lang->t('Delete'), ['delete', 'id' => $model->id_kurs], [
            'class' => 'btn btn-danger btn-xs',
            'data' => [
                'confirm' => Yii::$app->lang->t('Delete course confirmation'),
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a(Yii::$app->lang->t('Groups'), ['group/index', 'id_kurs' => $model->id_kurs], ['class' => 'btn btn-primary btn-xs']) ?>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_kurs',
            'name_kurs',
            'description:ntext',
            'visible:boolean',
        ],
    ]) ?>
    <div class="manager"> <h3><?=Yii::$app->lang->t('Admins')?></h3>
        <ol>
        <?php
         $users=$model->getUsers()->all();
         
         foreach($users as $user){
             echo  "<li>".Html::a(" {$user->userLogin}, {$user->userPIB} ", ['user/view', 'id' => $user->userId], ['class' => '', 'target'=>"_blank"]) ."</li>";
         }
        ?> 
        </ol>
    </div>    

    <h3><?=Yii::$app->lang->t('BlankAnkety')?></h3>
    <?php
       $decoded=json_decode($model->anketa,true);
       if($decoded){
           echo "<div style=\"color:green;\">".Yii::$app->lang->t('SyntaxOK')."</div>";
       }else{
           echo "<div style=\"color:red;\">".Yii::$app->lang->t('SyntaxError')."</div>";
       }
    ?>
    <pre><?=htmlspecialchars($model->anketa)?></pre>
    
    
    <?php
    if($decoded){

        $obj = new \app\lib\ShowAnketa('anketa');
        echo "<h3>".Yii::$app->lang->t('BlankAnketyPreview')."</h3>".$obj->form($decoded, []);    
        
        $this->registerCssFile(Yii::getAlias('@web/css/bootstrap-datepicker.min.css'));
        $this->registerJSFile(Yii::getAlias('@web/js/bootstrap-datepicker.min.js'),['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->registerJSFile(Yii::getAlias('@web/js/bootstrap-datepicker.ru.min.js'),['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->registerJSFile(Yii::getAlias('@web/js/showanketa.js'),['depends' => [\yii\web\JqueryAsset::className()]]); 

        $this->registerJs("
            window.alertShown=false;
            window.enableAlertDelayed=function(){window.alertShown=false;};
            $('#studentUpdateForm').submit(function(event){
               if(!anketa.fn.validate()){
                 if(!window.alertShown){
                    alert( '".Yii::$app->lang->t('FillInCorrectData')."' );
                    window.alertShown=true;
                    setTimeout(window.enableAlertDelayed, 1000);
                 }
                 event.preventDefault();
               }
            });
        ");


    }

    ?>
    
</div>
