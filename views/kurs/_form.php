<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Kurs */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kurs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name_kurs')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'visible')->checkbox() ?>

    <?php
    if(\app\models\User::isAdmin()){
        echo $form->field($model, 'userIds')->widget(Select2::classname(), [
            'data' => $userOptions,
            'options' => ['placeholder' => Yii::$app->lang->t('Select manager')],
            'pluginOptions' => [
                'allowClear' => true,
                'multiple' => true
            ],
        ]);        
    }
    ?>

    <?= $form->field($model, 'anketa')->textarea(['rows' => 6]) ?>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::$app->lang->t('Create') : Yii::$app->lang->t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
