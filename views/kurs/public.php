<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KursSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->lang->t('Kurs list');
$this->params['breadcrumbs'][] = Yii::$app->lang->t('Kurs list');
?>
<div class="kurs-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php
    
    
    foreach($rows as $row){
        ?><div>
            <h3><?=Html::a($row->name_kurs, ['/kurs/details', 'id' => $row->id_kurs], ['class' => '-'])?></h3>
            <div><?=$row->description?></div>
            <?php
                $now=date('Y-m-d');
                $groups=$row->getGroups()->andWhere(" visible=1 and date_start >= '$now' ")->all();
                foreach($groups as $group){
                    // echo '<pre>'; print_r($groups); echo '</pre>';
                    echo '<div class="group-item">'.$group->name_group.' '.Yii::$app->lang->t('Deadline').' '.date(Yii::$app->params['dateFormat'],strtotime($group->date_start))
                         .' '.Html::a(Yii::$app->lang->t('Signup'), ['/student/registration', 'id_group' => $group->id_group], ['class' => 'btn btn-success btn-xs'])
                         
                         . ( \Yii::$app->params['allow_corrections'] 
                            ? ( ' ' . Html::a(Yii::$app->lang->t('Update profile'), ['/student/reminder', 'id_group' => $group->id_group], ['class' => 'btn btn-primary btn-xs']) )
                            :'')

                        .'</div>';
                }
                
            ?>
        </div><?php
    }
    
    ?>

</div>
