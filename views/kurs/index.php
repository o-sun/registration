<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KursSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->lang->t('Kurs list');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kurs-index">

    <h1><?= Html::encode($this->title) ?> <?= Html::a(Yii::$app->lang->t('Create course'), ['create'], ['class' => 'btn btn-success btn-xs']) ?></h1>



    <?php
    if (\app\models\User::isAdmin()) {
        $actionTemplate = '{view}&nbsp;{update}{group}&nbsp;{delete}';
    } else {
        $actionTemplate = '{view}&nbsp;{update}{group}';
    }
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => $actionTemplate,
                'buttons' => [
                    'group' => function ($url, $model, $key) {
                        return '<b>' . Html::a(' <span class="glyphicon glyphicon-user"></span> ', ['group/index', 'id_kurs' => $model->id_kurs], ['title' => Yii::$app->lang->t('Groups')]) . '</b>';
                    }
                ]
            ],
            [
                'attribute' => 'id_kurs',
                'filterOptions' => ['class' => 'filterId'],
            ]
            ,
            'name_kurs',
            [
                'attribute' => 'visible',
                'format'=>'boolean',
                'filterOptions' => ['class' => 'filterId'],
                'filter'=>array(""=>"","1"=>"Yes","0"=>"No")
            ]
        ],
    ]);
    ?>

</div>
