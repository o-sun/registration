<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Kurs */

$this->title =  Yii::$app->lang->t('Update {name_kurs}',['name_kurs'=>$model->name_kurs]);
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kursy'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name_kurs, 'url' => ['view', 'id' => $model->id_kurs]];
$this->params['breadcrumbs'][] = Yii::$app->lang->t('Update');
?>
<div class="kurs-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'userOptions'=>$userOptions,
    ]) ?>

</div>
