<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Kurs */

$this->title = $model->name_kurs;

if($model->isKursManager()){
    $this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kursy'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->name_kurs, 'url' => ['view', 'id' => $model->id_kurs]];
}else{
    $this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kursy'), 'url' => ['public']];
}
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kurs-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= $model->description ?>
    </p>
    <?php
    $groups = $model->getGroups()->andWhere(' visible=1 and date_start > now() ')->all();
    foreach ($groups as $group) {
        // echo '<pre>'; print_r($groups); echo '</pre>';
        echo '<div class="group-item"> ' .Yii::$app->lang->t('Deadline').' '. date(Yii::$app->params['dateFormat'], strtotime($group->date_start))
        . ' ' . Html::a(Yii::$app->lang->t('Signup'), ['/student/registration', 'id_group' => $group->id_group], ['class' => 'btn btn-success btn-xs'])
        . ' ' . 
            ( \Yii::$app->params['allow_corrections']
                ? Html::a(Yii::$app->lang->t('Update profile'), ['/student/reminder', 'id_group' => $group->id_group], ['class' => 'btn btn-primary btn-xs'])
                : '' )
                
        . '</div>';
    }
    ?>
</div>
