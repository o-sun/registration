<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KursSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kurs-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_kurs') ?>

    <?= $form->field($model, 'name_kurs') ?>

    <?= $form->field($model, 'description') ?>

    <?= $form->field($model, 'anketa') ?>

    <?= $form->field($model, 'visible')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::$app->lang->t('Find'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::$app->lang->t('Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
