<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Group */
$this->title = Yii::$app->lang->t('Create group');
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kursy'), 'url' => ['/kurs/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kurs {name_kurs}',['name_kurs'=>($kurs?$kurs->name_kurs:'')]), 'url' => ['/kurs/view', 'id'=>$model->id_kurs]];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Groups'), 'url' => ['index', 'id_kurs'=>$model->id_kurs]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'kursOptions' => $kursOptions,
    ]) ?>

</div>
