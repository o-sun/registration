<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Group */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="group-form">

    <?php $form = ActiveForm::begin(); ?>

    
    <?=$form->field($model, 'id_kurs')->dropDownList($kursOptions, [])?>

    <?= $form->field($model, 'name_group')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'date_start')->textInput() ?>

    <?= $form->field($model, 'max_count_stud')->textInput() ?>

    <?= $form->field($model, 'visible')->checkbox() ?>

    <?= $form->field($model, 'anketa')->textarea(['rows' => 6]) ?>
    <p><?=Yii::$app->lang->t('group_anketa_manual')?></p>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::$app->lang->t('Create') : Yii::$app->lang->t('Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
