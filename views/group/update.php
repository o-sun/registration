<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Group */


$kurs=$model->idKurs;
$this->title = Yii::$app->lang->t('Update group') . ': ' . $model->id_group;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kurs list'), 'url' => ['/kurs/index']];
$this->params['breadcrumbs'][] = ['label' => ($kurs?$kurs->name_kurs:''), 'url' => ['/kurs/view', 'id'=>$model->id_kurs]];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Groups'), 'url' => ['index', 'id_kurs'=>$model->id_kurs]];
$this->params['breadcrumbs'][] = ['label' => $model->id_group, 'url' => ['view', 'id' => $model->id_group]];
$this->params['breadcrumbs'][] = Yii::$app->lang->t('Update');
?>
<div class="group-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'kursOptions' => $kursOptions,
    ]) ?>

</div>
