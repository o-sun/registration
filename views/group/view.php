<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Group */
$kurs=$model->idKurs;
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kursy'), 'url' => ['/kurs/index']];
$this->params['breadcrumbs'][] = ['label' => ($kurs?$kurs->name_kurs:''), 'url' => ['/kurs/view', 'id'=>$model->id_kurs]];
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Groups'), 'url' => ['index', 'id_kurs'=>$model->id_kurs]];
$this->title = $model->name_group;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-view">

    <h1><?= Html::encode($this->title) ?>

    
        <?= Html::a(Yii::$app->lang->t('Members'), ['/student/index', 'id_group' => $model->id_group], ['class' => 'btn btn-primary btn-xs']) ?>
        <?= Html::a(Yii::$app->lang->t('Registration'), ['/student/registration', 'id_group' => $model->id_group], ['class' => 'btn btn-primary btn-xs']) ?>
        <?= Html::a(Yii::$app->lang->t('Update'), ['update', 'id' => $model->id_group], ['class' => 'btn btn-primary btn-xs']) ?>
        <?= Html::a(Yii::$app->lang->t('Delete'), ['delete', 'id' => $model->id_group], [
            'class' => 'btn btn-danger btn-xs',
            'data' => [
                'confirm' => Yii::$app->lang->t('Delete group confirmation'),
                'method' => 'post',
            ],
        ]) ?>
    </h1>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_group',
            [                      // the owner name of the model
            'label' => Yii::$app->lang->t('Course'),
            'value' => $kurs->name_kurs,
        ],
            'name_group',
            'date_start',
            'max_count_stud',
            'visible:boolean',
            [
                'label'=> Yii::$app->lang->t('NumOfMembers'),
                'value'=>count($model->students)
            ]
        ],
    ]) ?>

</div>
