<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\GroupSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::$app->lang->t('Groups');
$this->params['breadcrumbs'][] = ['label' => Yii::$app->lang->t('Kurs list'), 'url' => ['/kurs/index']];
$this->params['breadcrumbs'][] = ['label' => ($kurs ? $kurs->name_kurs : '') . '"', 'url' => ['/kurs/view', 'id' => ($kurs ? $kurs->id_kurs : '')]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="group-index">

    <h1><?= Html::encode($this->title) ?> <?= Html::a(Yii::$app->lang->t('Create'), ['create', 'id_kurs' => $kurs->id_kurs], ['class' => 'btn btn-success btn-xs']) ?></h1>


    <?=
    GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{registration}&nbsp;{students}&nbsp;{view}&nbsp;{update}&nbsp;&nbsp;{delete}',
                'buttons' => [
                    'registration' => function ($url, $model, $key) {
                        return '<b>' . Html::a(' <span class="glyphicon glyphicon-heart"></span> ', ['/student/registration', 'id_group' => $model['id_group']], ['title' => Yii::$app->lang->t('Registration')]) . '</b>';
                    },
                    'students' => function ($url, $model, $key) {
                        return '<b>' . Html::a(' <span class="glyphicon glyphicon-user"></span> ', ['/student/index', 'id_group' => $model['id_group']], ['title' => Yii::$app->lang->t('Members')]) . '</b>';
                    },
                    'view' =>  function ($url, $model, $key) {
                        return '<b>' . Html::a(' <span class="glyphicon glyphicon-eye-open"></span> ', ['view', 'id' => $model['id_group']], ['title' => Yii::$app->lang->t('View')]) . '</b>';
                    },
                    'update' =>  function ($url, $model, $key) {
                        return '<b>' . Html::a(' <span class="glyphicon glyphicon-pencil"></span> ', ['update', 'id' => $model['id_group']], ['title' => Yii::$app->lang->t('Update')]) . '</b>';
                    },
                    'delete' =>  function ($url, $model, $key) {
                        return '<b>' . Html::a(' <span class="glyphicon glyphicon-trash"></span> ', 
                                               ['delete', 'id' => $model['id_group']], [
                                                'class' => ' ',
                                                'data' => [
                                                    'confirm' => Yii::$app->lang->t('Confirm removal'),
                                                    'method' => 'post',
                                                ]
                                               ] ).'</b>';
                    },
                ]
             ],
             [
                 'attribute'=>'id_group',
                 'label'=>Yii::$app->lang->t('Code'),
                 'filterOptions' => ['class' => 'filterId'],
             ],
            // 'id_kurs',
             [
                 'attribute'=>'name_kurs',
                 'label'=>Yii::$app->lang->t('Course')
             ],
             [
                 'attribute'=>'name_group',
                 'label'=>Yii::$app->lang->t('Group name')
             ],
             [
                 'attribute'=>'date_start',
                 'label'=>Yii::$app->lang->t('Deadline')
             ],
             [
                 'attribute'=>'count_stud',
                 'label'=>Yii::$app->lang->t('NumOfMembers'),
                 'filterOptions' => ['class' => 'filterId'],
             ],
             [
                 'attribute'=>'max_count_stud',
                 'label'=>Yii::$app->lang->t('maxNumOfMembers'),
                 'filterOptions' => ['class' => 'filterId'],
             ],
             [
                 'attribute'=>'visible',
                 'format'=>'boolean',
                 'label'=>Yii::$app->lang->t('isVisible'),
                 'filterOptions' => ['class' => 'filterId'],
             ],
        // 'anketa',
        ],
    ]);
    ?>

</div>
