<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\GroupSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="group-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_group') ?>

    <?= $form->field($model, 'id_kurs') ?>

    <?= $form->field($model, 'name_group') ?>

    <?= $form->field($model, 'date_start') ?>

    <?= $form->field($model, 'max_count_stud') ?>

    <?php // echo $form->field($model, 'visible')->checkbox() ?>

    <?php // echo $form->field($model, 'anketa') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::$app->lang->t('Find'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::$app->lang->t('Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
