<?php
use yii\helpers\Url;
echo Yii::$app->lang->t('statusupdate.php',[
    'id_stud'=>$student->id_stud,
    'firstName'=>$student->firstName,
    'middleName'=>$student->middleName,
    'lastName'=>$student->lastName,
    'name_kurs'=>$kurs->name_kurs,
    'name_group'=>$group->name_group,
    // 'newstatusTite'=>Url::toRoute (['/student/update-registration', 'id_stud' => $student->id_stud, 'secret_code'=>$student->secret_code], true),
    'newstatusTite'=>Yii::$app->lang->t('status_'.$student->status),
    'siteName'=>Yii::$app->params['siteName'],
    'homeUrl'=>Yii::$app->homeUrl,
]);
