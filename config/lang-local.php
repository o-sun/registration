<?php

return [
    'Kurs list' => 'Реєстрації',
    'Registration' => 'Реєстрація',
    'Kurs' => 'Реєстрації',
    'Operators' => 'Оператори',
    'Home' => 'Реєстрації',
    'userIsActive' => 'Активний',
    'userLastLogin' => 'Час останнього входу',
    'userContactData' => 'Контактні дані (тел., адреса)',
    'userPIB' => "Прізвище, ім'я, по батькові",
    'userPassword' => 'Пароль',
    'userId' => 'Код',
    'userLogin' => 'Логін',
    'idKurs' => 'Реєстрації',
    'id_stud' => 'Код анкети',
    'id_group' => 'Група',
    'firstName' => "Ім'я",
    'middleName' => 'По батькові',
    'lastName' => 'Прізвище',
    'email' => 'Email',
    'secret_code' => 'Секретний код',
    'anketa' => 'Анкета',
    'username' => "Ім'я",
    'password' => 'Пароль',
    'rememberMe' => "Запам'ятати",
    'id_kurs' => 'Код',
    'name_kurs' => 'Назва реєстрації',
    'description' => 'Опис',
    'anketa' => 'Бланк анкети',
    'visible' => 'Видимість',
    'userIds' => 'Адміністратори',
    'id_group' => 'Код',
    'id_kurs' => 'Реєстрація',
    'name_group' => 'Назва групи',
    'date_start' => 'Реєстрація до',
    'max_count_stud' => 'Максимальна кількість учасників',
    'visible' => 'Видимість',
    'anketa' => 'Бланк анкети',
    'Login' => 'Увійти',
    //
    //
    'reminder.php' =>
    "Доброго дня, {firstName} {middleName} {lastName}

Ви зареєстровані в {name_kurs}
В групу {name_group}
З кодом {id_stud}

Для зміни своїх даних використвуйте посилання
{update_url}
Щоб запобігти втраті персональних даних, нікому не передавайте це посилання.

З повагою,
адміністрація сайта
{siteName}

", //{homeUrl}
    //
    //
    'confirmation.php' => "Доброго дня, {firstName} {middleName} {lastName}

Ви зареєстровані в {name_kurs}
В групу {name_group}
З кодом {id_stud}

З повагою,
адміністрація сайта
{siteName}
", //{homeUrl}
    //Для зміни своїх даних використвуйте посилання
    //{update_url}
    //Щоб запобігти втраті персональних даних, нікому не передавайте це посилання.
    
    'Select one of {label}' => "Виберіть один із запропонованих варіантів {label}",
    'String {value} is not recognized as date' => "Рядок {value} не відповідає формату дати",
    'Enter date {label}' => "Введіть дату {label}",
    "Error {error} while uploading file {label}" => "Помилка {error}  завантаження файла {label} ",
    "Select value of {label}" => "Виберіть значення {label}",
    "Fill in the {label}" => "Заповніть поле {label}",
    'Admin rights required' => 'Ви маєте бути адміністратором, щоб виконувати цю дію',
    'Admin/manager rights required' => 'Ви маєте бути секретарем або адміністратором, щоб виконувати цю дію',
    'Page not found' => 'Запитана вами сторінка не існує.',
    'firstName' => "Ім'я",
    'middleName' => 'По батькові',
    'lastName' => 'Прізвище',
    "{siteName}: registration to {name_kurs}"=>"{siteName}: реєстрація {name_kurs}",
    "Secure link posted to {email}"=>"Секретне посилання надіслане вам за адресою {email}",
    'Registration finished'=>'Реєстрація закінчена.',
    'Confirm removal'=>'Ви дійсно бажаєте видалити дані?',
    'Management'=>'Керування',
    'Email {email} not found'=>"Участник із електронною поштою {email} не знайдений",
    'Email {email} already taken'=>"Участник із електронною поштою  {email} вже зареєстрований",
    'Reset'=>'Відмінити',
    'Find'=>'Знайти',
    'group_anketa_manual'=>'Заповнюйте бланк анкети тільки якщо вас не влаштовує анкета у властивостях реєстрації',
    'Create'=>'Створити',
    'Update'=>'Змінити',
    'Create group'=>'Створити групу',
    'Kursy'=>'Реєстрації',
    'Kurs {name_kurs}'=>'Реєстрація "{name_kurs}"',
    'Groups'=>'Групи',
    'Members'=>'Учасники',
    'View'=>'Перегляд',
    'Course'=>'Реєстрація',
    'Code'=>'Код',
    'Group name'=>'Назва групи',
    'Deadline'=>'Реєстрація до',
    'NumOfMembers'=>'Учасників',
    'maxNumOfMembers'=>'Макс. учасників',
    'isVisible'=>'Видима',
    'Delete group confirmation'=>'Ви впевнені, що хочете видалити цю групу?',
    'Select manager'=>'Виберіть секретаря ...',
    'Create course'=>'Створит реєстрацію',
    'Delete'=>'Видалити',
    'Signup'=>'Зареєструватися',
    'Update profile'=>'Змінити реєстрацію',
    'Update {name_kurs}'=>'Змінити реєстрацію: {name_kurs}',
    'Delete course confirmation'=>'Ви дійсно хочете видалити цю реєстрацію?',
    'Admins'=>'Адміністратори',
    'BlankAnkety'=>'Бланк анкети',
    'SyntaxOK'=>'Синтаксис перевірено',
    'SyntaxError'=>'Помилка синтаксису',
    'BlankAnketyPreview'=>'Внешний вид анкеты',
    'LoginPage'=>'Вхід на сайт',
    'FillInCorrectData'=>'Будь ласка, заповніть форму правильно',
    'RegistrationCancelled'=>'Реєстрація скасована',
    'Group {name_group}'=>'Група {name_group}',
    'RegistrationCancel'=>'Скасувати реєстрацію',
    'Group'=>'Група',
    'Add member'=>'Додати учасника',
    'Download members'=>'Скачати учасників',
    'Users'=>'Користувачі',
    'User'=>'Користувач',
    'Reminder'=>'Отримання секретного посилання',
    'Your email'=>'Ваша електрона пошта',
    'GetSecureLink'=>'Отримати секретне посилання',
    'SecureLinkHint'=>'Відкривши секретне посилання до {date}, ви зможете змінити або видалити заповнену анкету.',
    'Update member'=>'Змінити учасника',
    'Basic data'=>'Основні дані',
    'Update password'=>'Змінити пароль',
    'Type new password'=>'Новий пароль',
    'Retype new password'=>'Новий пароль ще раз',
    'Select courses'=>'Виберіть курси ...',
    'Update group'=>'Змінити групу',
    'Select group'=>'Виберіть групу ...',
    'dateCreated'=>'Дата реєстрації',
    'keyWords'=>'Слова',
    'status'=>'Стан заяви',
    'status_new'=>'Нова',
    'status_rejected'=>'Відхилена',
    'status_accepted'=>'Схвалена',
    '{siteName}: registration status updated {name_kurs}'=>'{siteName}: змінено стан заяви на {name_kurs}',
    'statusupdate.php'=>"Доброго дня, {firstName} {middleName} {lastName}

Ви зареєстровані в {name_kurs}
В групу {name_group}
З кодом {id_stud}

Стан вашої заяви змінено на {newstatusTite}

З повагою,
адміністрація сайта
{siteName}
",
    //{homeUrl}
    // ''=>'',
];
//Yii::$app->lang->t('')

