<?php

$params = require(__DIR__ . '/params.php');
if (file_exists(__DIR__ . '/params-local.php')) {
    $params = array_merge($params, require(__DIR__ . '/params-local.php'));
}
$db = require(__DIR__ . '/db.php');
if (file_exists(__DIR__ . '/db-local.php')) {
    $db = array_merge($db, require(__DIR__ . '/db-local.php'));
}
$lang = require(__DIR__ . '/lang.php');
if (file_exists(__DIR__ . '/lang-local.php')) {
    $lang = array_merge($db, require(__DIR__ . '/lang-local.php'));
}
$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'defaultRoute' => 'kurs/public',
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'qwerttyasdfghjzxcvvbbnkjer78gjb0',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'lang' => [
            'class' => 'app\lib\Lang',
            'messages' => $lang,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => '10.1.100.153',  // e.g. smtp.mandrillapp.com or smtp.gmail.com
                'username' => 'webmaster',
                'password' => '12we56ym',
                'port' => '25', // Port 25 is a very common port too
                // 'encryption' => 'tls', // It is often used, check your provider or mail server specs
            ]
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
