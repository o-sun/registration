<?php

return [
    'siteName' => 'Реєстрація ЗНУ',
    'adminEmail' => 'webmaster@znu.edu.ua',
    'salt'=>'jKLl897&^65wegkjlJKL:ijiu56hklkj123409',
    'rowsPerPage'=>10,
    'dateFormat'=>'d.m.Y',
    'admins'=>['admin'],
    'allow_corrections'=>false
];
