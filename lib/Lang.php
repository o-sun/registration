<?php

namespace app\lib;

/**
 * Description of Lang
 */
class Lang {
    public $messages;
    public function t($msg, $params=[]){
        if(!$this->messages){
            $this->messages=[];
        }
        if(isset($this->messages[$msg])){
            $template=$this->messages[$msg];
        }else{
            $template=$msg;
        }
        $from=  array_map(function($k){return "{{$k}}";},array_keys($params));
        $to=array_values($params);
        return str_replace($from, $to, $template);
    }
}
