<?php

/**
 * Description of ShowAnketa
 *
 * @author OV
 */

namespace app\lib;

use Yii;

class ShowAnketa {

    private $tegi = [];
    private $input = [];
    private $views = [];
    private $validator = [];
    private $formName;

    public function __construct($formName) {
        $this->formName = $formName;


        // =================== draw form elements = begin ======================
        $this->draw["row"] = function($arg, $dataAnketa = []) {
            if(!isset($arg['class'])){
                $arg['class']='';
            }
            $value = is_array($arg["value"]) ? join("", $arg["value"]) : $arg["value"];
            return "<div class=\"row {$arg['class']}\">{$value}</div>";
        };
        $this->draw["column"] = function($arg, $dataAnketa = []) {
            if(!isset($arg['class'])){
                $arg['class']='';
            }
            $value = is_array($arg["value"]) ? join("", $arg["value"]) : $arg["value"];
            return "<div class='col-md-{$arg['width']}  {$arg['class']}'>{$value}</div>";
        };
        $this->draw["html"] = function($arg, $dataAnketa = []) {
            if(!isset($arg['class'])){
                $arg['class']='';
            }
            return "<div class='html {$arg['class']}'>{$arg['html']}</div>";
        };
        $this->draw["textfield"] = function($arg, $dataAnketa = []) {
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']] : '';

            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }
            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            if (!isset($arg['tip'])) {
                $arg['tip'] = '';
            }
            if(!isset($arg['class'])){
                $arg['class']='';
            }
            if(!isset($arg['placeholder'])){
                $arg['placeholder']='';
            }
            return "<div class='textfield form-group'>" .
                    ( strlen($arg['label'])>0 ? ( "<label>{$arg['label']} " . ($arg['required'] ? '*' : '') . " </label>" ):'' )
                    . "<input type=\"text\" class=\"form-control anketa-textfield " . ($arg['required'] ? 'required' : '') . "  {$arg['class']}\" 
                       name=\"{$this->formName}[{$arg['name']}]\" 
                       value=\"" . htmlspecialchars($arg['value'] ? $arg['value'] : $arg['default']) . "\" 
                       placeholder=\"".  htmlspecialchars($arg['placeholder'])."\"
                       aria-describedby=\"sizing-addon1\">"
                    . "<span class='message'></span>"
                    . "<div class='tip'>{$arg['tip']}</div>"
                    . "</div>";
        };
        $this->draw["textarea"] = function($arg, $dataAnketa = []) {
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']] : '';

            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            if (!isset($arg['tip'])) {
                $arg['tip'] = '';
            }
            if(!isset($arg['class'])){
                $arg['class']='';
            }
            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }

            return "<div class='textarea form-group'>" .
                    ( strlen($arg['label'])>0 ? ( "<label>{$arg['label']} " . ($arg['required'] ? '*' : '') . " </label>" ):'' )
                    . "<textarea class=\"form-control anketa-textarea " . ($arg['required'] ? 'required' : '') . "  {$arg['class']}\" 
                       name=\"{$this->formName}[{$arg['name']}]\" >" . htmlspecialchars($arg['value'] ? $arg['value'] : $arg['default']) . "</textarea>"
                    . "<span class='message'></span>"
                    . "<div class='tip'>{$arg['tip']}</div>"
                    . "</div>";
        };
        $this->draw["checkbox"] = function($arg, $dataAnketa = []) {
            $arg['checked'] = isset($dataAnketa[$this->formName][$arg['name']]) ? true : false;
            if (is_bool($arg['checked'])) {
                $checked = ($arg['checked'] === true) ? 'checked' : '';
            } else {
                $checked = ($arg['default'] === true) ? 'checked' : '';
            }

            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            if (!isset($arg['tip'])) {
                $arg['tip'] = '';
            }
            if(!isset($arg['class'])){
                $arg['class']='';
            }
            if(!isset($arg['value'])){
                $arg['value']='on';
            }

            return "<div class='checkbox form-group'>"
                    . "<label><input type=\"checkbox\"  class=\"anketa-checkbox " . ($arg['required'] ? 'required' : '') . "  {$arg['class']}\"
                       name=\"{$this->formName}[{$arg['name']}]\" value=\"" . htmlspecialchars($arg['value']) . "\" 
                       {$checked} >{$arg['label']} " . ($arg['required'] ? '*' : '') . " </label>"
                    . "<div class='message'></div>"
                    . "<div class='tip'>{$arg['tip']}</div>"
                    . "</div>";
        };
        $this->draw["file"] = function($arg, $dataAnketa = []) {
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']] : '';

            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            if (!isset($arg['tip'])) {
                $arg['tip'] = '';
            }
            if(!isset($arg['class'])){
                $arg['class']='';
            }
            
            if(!isset($arg['max-size'])){
                $arg['max-size']=false;
            }
            if($arg['max-size']){
                if( preg_match('/k$/i', $arg['max-size'])){
                    $arg['max-size']*=1024;
                }elseif( preg_match('/m$/i', $arg['max-size'])){
                    $arg['max-size']*=1024*1024;
                }elseif( preg_match('/g$/i', $arg['max-size'])){
                    $arg['max-size']*=1024*1024*1024;
                }else{
                    $arg['max-size']*=1;
                }
            }
            if(!isset($arg['extension'])){
                $arg['extension']=false;
            }
            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }

            
            return "<div class='file form-group'>" .
                    ( strlen($arg['label'])>0 ? ( "<label>{$arg['label']} " . ($arg['required'] ? '*' : '') . " </label>" ):'' )
                    . "<input type=\"file\" 
                              class=\"form-control anketa-file " . ($arg['required'] ? 'required' : '') . "  {$arg['class']}\" 
                              name=\"{$this->formName}[{$arg['name']}]\" 
                              style=\"height:auto;\" 
                              ".( $arg['extension'] ? "data-extension=\"{$arg['extension']}\"":"")."
                              ".( $arg['max-size']?"data-max-size=\"{$arg['max-size']}\"":'' )."
                              data-prev=\"{$arg['name']}.file\">"
                    . "<input type='hidden' name=\"{$arg['name']}.file\" value=\"" . htmlspecialchars($arg['value']) . "\" >"
                    . ( strlen($arg['value']) > 0 ? ( "<a href=\"" . Yii::getAlias("@web/uploads/{$arg['value']}") . "\" target=\"_blank\">{$arg['value']}</a>" ) : '')
                    . "<span class='message'></span>"
                    . "<div class='tip'>{$arg['tip']}</div>"
                    . "</div>";
        };
        $this->draw["hidden"] = function($arg, $dataAnketa = []) {
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']] : '';
            return "<input type=\"hidden\" name=\"{$this->formName}[{$arg['name']}]\" value=\"" . htmlspecialchars($arg['value']) . "\" >";
        };
        $this->draw["date"] = function($arg, $dataAnketa = []) {
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']] : '';

            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            if (!isset($arg['tip'])) {
                $arg['tip'] = '';
            }
            if(!isset($arg['class'])){
                $arg['class']='';
            }
            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }

            return "<div class='date form-group'>" .
                    ( strlen($arg['label'])>0 ? ( "<label>{$arg['label']} " . ($arg['required'] ? '*' : '') . " </label>" ):'' )
                    . "<input type=\"text\" 
                              class=\"form-control anketa-date " . ($arg['required'] ? 'required' : '') . "  {$arg['class']}\" 
                              placeholder=\"".htmlspecialchars($arg['tip'])."\"
                              name=\"{$this->formName}[{$arg['name']}]\" 
                              value=\"" . htmlspecialchars($arg['value'] ? $arg['value'] : $arg['default']) . "\" >"
                    . "<span class='message'></span>"
                    . "<div class='tip'>{$arg['tip']}</div>"
                    . "</div>";
        };
        $this->draw["radio"] = function($arg, $dataAnketa = []) {
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']] : '';

            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            if (!isset($arg['tip'])) {
                $arg['tip'] = '';
            }
            if(!isset($arg['class'])){
                $arg['class']='';
            }
            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }

            $html = "<div class='radio form-group'>" .
                    ( strlen($arg['label'])>0 ? ( "<div><label class=\"grouplabel\">{$arg['label']} " . ($arg['required'] ? '*' : '') . " </label></div>" ):'' )
                    ;
            $value = $arg['value'] ? $arg['value'] : $arg['default'];
            foreach ($arg['items'] as $key => $val) {
                $checked = ( $key == $value ) ? 'checked' : '';
                $html.="<label class=\"itemlabel\"><input type=\"radio\" class=\"anketa-radio " . ($arg['required'] ? 'required' : '') . "  {$arg['class']}\" {$checked} "
                        . " name=\"{$this->formName}[{$arg['name']}]\" value=\"" . htmlspecialchars($key) . "\" > {$val}</label>";
            }
            $html.="<div class='message'></div>";
            $html.="<div class='tip'>{$arg['tip']}</div>";
            $html.="</div>";
            return $html;
        };
        $this->draw["select"] = function($arg, $dataAnketa = []) {
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']] : '';

            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            if (!isset($arg['tip'])) {
                $arg['tip'] = '';
            }
            if (!isset($arg['multiple'])) {
                $arg['multiple'] = false;
            }
            if(!isset($arg['class'])){
                $arg['class']='';
            }
            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }

            $html = "<div class='select form-group'>" 
                    . ( strlen($arg['label'])>0 ? ( "<div><label class=\"grouplabel\">{$arg['label']} " . ($arg['required'] ? '*' : '') . " </label></div>" ):'' )
                    . "<select name=\"{$this->formName}[{$arg['name']}]".($arg['multiple']?'[]" ':'')."\" "
                    . "        class=\"form-control anketa-select " . ($arg['required'] ? 'required' : '') . "  {$arg['class']}\""
                    . "        ".($arg['multiple']?' multiple="multiple" ':'').">"
                    . " <option value=''></option>";
            $value = $arg['value'] ? $arg['value'] : $arg['default'];
            if(is_array($value)){
                foreach ($arg['items'] as $key => $val) {
                    $selected = in_array( $key, $value ) ? 'selected' : '';
                    $html.="<option {$selected} value=\"" . htmlspecialchars($key) . "\" > {$val} </option>";
                }
            }else{
                foreach ($arg['items'] as $key => $val) {
                    $selected = ( $key == $value ) ? 'selected' : '';
                    $html.="<option {$selected} value=\"" . htmlspecialchars($key) . "\" > {$val} </option>";
                }
            }
            $html.="</select> "
                . "<span class='message'></span>"
                . "<div class='tip'>{$arg['tip']}</div> </div>";
            return $html;
        };
        $this->draw["tree"] = function($arg, $dataAnketa = []) {
            
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']] : '';

            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }

            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }

            if (!isset($arg['tip'])) {
                $arg['tip'] = '';
            }

            if(!isset($arg['class'])){
                $arg['class']='';
            }

            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }
            
            if(!isset($arg['tree']) || !is_array($arg['tree'])){
                $arg['tree']=['label'=>'', 'value'=>''];
            }

            $value = preg_replace("/^\\//","",$arg['value'] ? $arg['value'] : $arg['default']);
            $html = "<div class='select form-group'>" 
                    . ( strlen($arg['label'])>0 ? ( "<div><label class=\"grouplabel\">{$arg['label']} " . ($arg['required'] ? '*' : '') . " </label></div>" ):'' )
                    . " 
                        <script type=\"application/javascript\">
                        window.anketa = window.anketa || {};
                        anketa.tree=anketa.tree || {};
                        anketa.tree[\"tree-{$arg['name']}\"]={
                                containerId:\"#tree-{$arg['name']}\",
                                name:\"{$this->formName}[{$arg['name']}]\",
                                value:".json_encode(explode('/',$arg['value'])).",
                                items:".json_encode($arg['items']).",
                                required:".($arg['required'] ? 'true' : 'false')."
                        };
                       </script>
                       <div id=\"tree-{$arg['name']}\"></div>
                       ";
            $html.=   "<span class='message'></span>"
                    . "<div class='tip'>{$arg['tip']}</div>"
                    . "</div>";
            return $html;
        };
        // =================== draw form elements = end ========================
        // 
        // 
        // 
        // 
        // 
        // 
        // 
        // =================== get form  inputs = begin ========================

        $this->input["row"] = function($arg, $request = [], $files = []) {
            $inputs = [];
            foreach ($arg["value"] as $k => $v) {
                if (isset($v['teg'])) {
                    $inputs[$k] = $v;
                } else {
                    $inputs = array_merge($inputs, $v);
                }
            }
            return $inputs;
        };
        $this->input["column"] = $this->input["row"];


        $this->input["html"] = function($arg, $request = [], $files = []) {
            return [];
        };

        $this->input["textfield"] = function($arg, $request = [], $files = []) {
            $arg['value'] = isset($request[$this->formName][$arg['name']]) ? $request[$this->formName][$arg['name']] : '';
            if (!isset($arg['label'])) {
                $arg['label'] = $arg['name'];
            }
            $errors = [];
            if (isset($arg['required']) && $arg['required']) {
                $arg['value'] = trim($arg['value']);
                if (strlen($arg['value']) == 0) {
                    $errors[] = Yii::$app->lang->t("Fill in the {label}",['label'=>$arg['label']]) ;
                }
            }

            return [
                $arg['name'] => [
                    'name' => $arg['name'],
                    'teg' => $arg['teg'],
                    'value' => $arg['value'],
                    'required' => (isset($arg['required']) && $arg['required']),
                    'label' => $arg['label'],
                    'errors' => $errors,
                ]
            ];
        };
        $this->input["textarea"] = $this->input["textfield"];


        $this->input["checkbox"] = function($arg, $request = [], $files = []) {

            $arg['checked'] = isset($request[$this->formName][$arg['name']]) ? true : false;

            if(!isset($arg['value'])){
                $arg['value']=1;
            }
            $errors = [];
            if (isset($arg['required']) && $arg['required']) {
                if (!$arg['checked']) {
                    $errors[] = Yii::$app->lang->t("Select value of {label}",['label'=>$arg['label']]);
                }
            }

            return [
                $arg['name'] => [
                    'name' => $arg['name'],
                    'teg' => $arg['teg'],
                    'value' => $arg['value'],
                    'required' => (isset($arg['required']) && $arg['required']),
                    'label' => $arg['label'],
                    'errors' => $errors,
                    'checked' => $arg['checked'],
                ]
            ];
        };
        $this->input["file"] = function($arg, $request = [], $files = []) {
            
            //$_FILES['userfile']['name']
            //    Оригинальное имя файла на компьютере клиента.
            //$_FILES['userfile']['type']
            //    Mime-тип файла, в случае, если браузер предоставил такую информацию. Пример: "image/gif". Этот mime-тип не проверяется в PHP, так что не полагайтесь на его значение без проверки.
            //$_FILES['userfile']['size']
            //    Размер в байтах принятого файла.
            //$_FILES['userfile']['tmp_name']
            //    Временное имя, с которым принятый файл был сохранен на сервере.
            //$_FILES['userfile']['error']
            //    Код ошибки, которая может возникнуть при загрузке файла.
            if (!isset($arg['label'])) {
                $arg['label'] = $arg['name'];
            }

            $errors = [];
            if (isset($files[$this->formName]['name'][$arg['name']])) {
                $value = [
                    'name'     => $files[$this->formName]['name'][$arg['name']],
                    'type'     => $files[$this->formName]['type'][$arg['name']],
                    'size'     => $files[$this->formName]['size'][$arg['name']],
                    'tmp_name' => $files[$this->formName]['tmp_name'][$arg['name']],
                    'error'    => $files[$this->formName]['error'][$arg['name']]
                    ];
            } else {
                $value = ['name' => '', 'type' => '', 'size' => 0, 'tmp_name' => '', 'error' => UPLOAD_ERR_NO_FILE];
            }

            if (isset($arg['required']) && $arg['required']) {
                if ($value['size'] == 0 || $value['error'] != UPLOAD_ERR_OK) {
                    $errors[] = Yii::$app->lang->t("Error {error} while uploading file {label}",[ 'error'=>$value['error'],'label'=>$arg['label'] ]);
                }
            }
            return [
                $arg['name'] => [
                    'name' => $arg['name'],
                    'teg' => $arg['teg'],
                    'value' => $value['tmp_name'],
                    'required' => (isset($arg['required']) && $arg['required']),
                    'label' => $arg['label'],
                    'errors' => $errors,
                ]
            ];
        };
        

        $this->input["hidden"] = function($arg, $request = [], $files = []) {
            return [
                $arg['name'] => [
                    'name' => $arg['name'],
                    'teg' => $arg['teg'],
                    'value' => $arg['value'],
                    'errors' => [],
                ]
            ];
        };

        $this->input["date"] = function($arg, $request = [], $files = []) {
            $arg['value'] = isset($request[$this->formName][$arg['name']]) ? trim($request[$this->formName][$arg['name']]) : '';
            if (!isset($arg['label'])) {
                $arg['label'] = $arg['name'];
            }
            $errors = [];
            if (isset($arg['required']) && $arg['required']) {
                if (strlen($arg['value']) == 0) {
                    $errors[] = Yii::$app->lang->t('Enter date {label}',['label'=>$arg['label']]);
                }
            }
            if (($timestamp = strtotime($arg['value'])) === false) {
                $errors[] = Yii::$app->lang->t('String {value} is not recognized as date',['value'=>$arg['value']]);
            }
            return [
                $arg['name'] => [
                    'name' => $arg['name'],
                    'teg' => $arg['teg'],
                    'value' => $arg['value'],
                    'required' => (isset($arg['required']) && $arg['required']),
                    'label' => $arg['label'],
                    'errors' => $errors,
                ]
            ];
        };

        $this->input["radio"] = function($arg, $request = [], $files = []) {
            if (!isset($arg['label'])) {
                $arg['label'] = $arg['name'];
            }

            $arg['value'] = isset($request[$this->formName][$arg['name']]) ? $request[$this->formName][$arg['name']] : '';
            if (!isset($arg['default'])){
                $arg['default']='';
            }
            $value = $arg['value'] ? $arg['value'] : $arg['default'];
            $errors = [];
            if (isset($arg['required']) && $arg['required']) {
                if(is_array($value)){
                    if(count($value)==0){
                        $errors[] = Yii::$app->lang->t('Select one of {label}',['label'=>$arg['label']]);
                    }
                }elseif (strlen($value) == 0) {
                    $errors[] = Yii::$app->lang->t('Select one of {label}',['label'=>$arg['label']]);
                }
            }
            if(is_array($value)){
                foreach($value as $val){
                    if ($val && !isset($arg['items'][$val])) {
                        $errors[] = Yii::$app->lang->t('Select one of {label}',['label'=>$arg['label']]);
                        break;
                    }
                }
            }else{
                if ($value && !isset($arg['items'][$value])) {
                    $errors[] = Yii::$app->lang->t('Select one of {label}',['label'=>$arg['label']]);
                }
            }
            return [
                $arg['name'] => [
                    'name' => $arg['name'],
                    'teg' => $arg['teg'],
                    'value' => $arg['value'],
                    'required' => (isset($arg['required']) && $arg['required']),
                    'label' => $arg['label'],
                    'errors' => $errors,
                ]
            ];
        };
        $this->input["select"] = $this->input["radio"];
        $this->input["tree"] = function($arg, $request = [], $files = []) {
            $arg['value'] = isset($request[$this->formName][$arg['name']]) ? $request[$this->formName][$arg['name']] : '';
            $arg['value'] = preg_replace("/^\\//","",$arg['value']);
            
            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }
            
            if (!isset($arg['label'])) {
                $arg['label'] = $arg['name'];
            }
            
            if(!isset($arg['tree']) || !is_array($arg['tree'])){
                $arg['tree']=['label'=>'', 'value'=>''];
            }
            
            $errors = [];
            if (isset($arg['required']) && $arg['required']) {
                $arg['value'] = trim($arg['value']);
                if (strlen($arg['value']) == 0 || preg_match("/\\/\$/",$arg['value'])) {
                    $errors[] = Yii::$app->lang->t("Fill in the {label}",['label'=>$arg['label']]) ;
                }
            }

            
            return [
                $arg['name'] => [
                    'name' => $arg['name'],
                    'teg' => $arg['teg'],
                    'value' => $arg['value'],
                    'required' => (isset($arg['required']) && $arg['required']),
                    'label' => $arg['label'],
                    'errors' => $errors,
                ]
            ];

        };

        // =================== get form  inputs = end ==========================
        // 
        // 
        // 
        // 
        // 
        // 
        // 
        // =================== view anketa = begin =============================
        $this->views["row"] = function($arg, $dataAnketa = []) {
            $value = is_array($arg["value"]) ? join("", $arg["value"]) : $arg["value"];
            return "<div class=row>{$value}</div>";
        };
        $this->views["column"] = function($arg, $dataAnketa = []) {
            $value = is_array($arg["value"]) ? join("", $arg["value"]) : $arg["value"];
            return "<div class='col-md-{$arg['width']}'>{$value}</div>";
        };
        $this->views["html"] = function($arg, $dataAnketa = []) {
            return "<div class='html'>{$arg['html']}</div>";
        };
        $this->views["textfield"] = function($arg, $dataAnketa = []) {
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']] : '';

            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            if (!isset($arg['tip'])) {
                $arg['tip'] = '';
            }
            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }

            return "<div class='textfield form-group'>"
                    . ( strlen($arg['label'])>0 ? ( "<label>{$arg['label']} " . ($arg['required'] ? '*' : '') . " </label>" ):'' )
                    . "<div class=\" " . ($arg['required'] ? 'required' : '') . "\"> "
                    . htmlspecialchars($arg['value'] ? $arg['value'] : $arg['default']) 
                    . "</div>"
                    . "<div class='tip'>{$arg['tip']}</div>"
                    . "</div>";
        };

        $this->views["textarea"] = $this->views["textfield"];

        $this->views["checkbox"] = function($arg, $dataAnketa = []) {
            $arg['checked'] = isset($dataAnketa[$this->formName][$arg['name']]) ? true : false;
            if (is_bool($arg['checked'])) {
                $checked = ($arg['checked'] === true) ? 'checked' : '';
            } else {
                $checked = ($arg['default'] === true) ? 'checked' : '';
            }
            if (!isset($arg['value'])) {
                $arg['value'] = 1;
            }
            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            if (!isset($arg['tip'])) {
                $arg['tip'] = '';
            }

            return "<div class='checkbox form-group'>" .
                    " "
                    . "<input type=\"checkbox\" disabled=\"true\" class=\"" . ($arg['required'] ? 'required' : '') . "\"
                       name=\"{$this->formName}[{$arg['name']}]\" value=\"" . htmlspecialchars($arg['value']) . "\" 
                       {$checked} > {$arg['label']} " . ($arg['required'] ? '*' : '') . " "
                    . "<div class='tip'>{$arg['tip']}</div>"
                    . "</div>";
        };
        $this->views["file"] = function($arg, $dataAnketa = []) {
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']] : '';

            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            if (!isset($arg['tip'])) {
                $arg['tip'] = '';
            }
            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }

            return "<div class='file form-group'>"
                    . ( strlen($arg['label'])>0 ? ( "<label>{$arg['label']} " . ($arg['required'] ? '*' : '') . " </label>" ):'' )
                    . ( strlen($arg['value']) > 0 ? ( "<a href=\"" . Yii::getAlias("@web/uploads/{$arg['value']}") . "\" target=\"_blank\">{$arg['value']}</a>" ) : '')
                    . "<div class='tip'>{$arg['tip']}</div>"
                    . "</div>";
        };
        $this->views["hidden"] = function($arg, $dataAnketa = []) {
            // $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']] : '';
            //return "<input type=\"hidden\" name=\"{$this->formName}[{$arg['name']}]\" value=\"" . htmlspecialchars($arg['value']) . "\" >";
            return "";
        };
        $this->views["date"] = function($arg, $dataAnketa = []) {
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']] : '';

            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            if (!isset($arg['tip'])) {
                $arg['tip'] = '';
            }
            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }

            $timestamp = strtotime($arg['value']);
            return "<div class='date form-group'>" 
                    . ( strlen($arg['label'])>0 ? ( "<label>{$arg['label']} " . ($arg['required'] ? '*' : '') . " </label><br>" ):'' )
                    . ( ( $timestamp !== false) ? date(Yii::$app->params['dateFormat'], $timestamp) : '')
                    . "<div class='tip'>{$arg['tip']}</div>"
                    . "</div>";
        };
        $this->views["radio"] = function($arg, $dataAnketa = []) {
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']] : '';

            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            if (!isset($arg['tip'])) {
                $arg['tip'] = '';
            }
            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }

            $html = "<div class='radio form-group'>" 
                    . ( strlen($arg['label'])>0 ? ( "<label class=\"grouplabel\">{$arg['label']} " . ($arg['required'] ? '*' : '') . " </label><br>" ):'' )
                    ;
            $value = $arg['value'] ? $arg['value'] : $arg['default'];
            if(is_array($value)){
                foreach ($arg['items'] as $key => $val) {
                    if (in_array($key, $value)) {
                        $html.="<label class=\"itemlabel\"> {$val}</label>";
                    }
                }
            }else{
                foreach ($arg['items'] as $key => $val) {
                    if ($key == $value) {
                        $html.="<label class=\"itemlabel\"> {$val}</label>";
                    }
                }
            }
            $html.="<div class='tip'>{$arg['tip']}</div>";
            $html.="</div>";
            return $html;
        };
        $this->views["select"] = $this->views["radio"];
        
        
        $this->views["tree"] = function($arg, $dataAnketa = []) {
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']] : $arg['default'];
            $arg['value'] = preg_replace("/^\\//","",$arg['value']);

            if (!isset($arg['required'])) {
                $arg['required'] = false;
            }
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            if (!isset($arg['tip'])) {
                $arg['tip'] = '';
            }
            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }

            $html = "<div class='tree form-group'>" 
                    . ( strlen($arg['label'])>0 ? ( "<label class=\"grouplabel\">{$arg['label']} " . ($arg['required'] ? '*' : '') . " </label><br>" ):'' )
                    ;
                    
            $value = explode("/",$arg['value']);

            $items=$arg['items'];
            foreach($value as $val){
                if(isset($items[$val])){
                    $html.="<label class=\"treeNodeLabel\"> {$items[$val]['label']}</label>";
                    if(isset($items[$val]['items']) && is_array($items[$val]['items']) ){
                        $items=$items[$val]['items'];
                    }else{
                        break;
                    }
                }else{
                    break;
                }
            }
            $html.="<div class='tip'>{$arg['tip']}</div>";
            $html.="</div>";
            return $html;
        };
        // =================== view anketa = end ===============================
        // 
        // 
        // 
        // 
        // 
        // 
        // 
        // =================== cellview anketa = begin =========================
        $this->cellviews["row"] = function($arg, $dataAnketa = []) {
            $inputs = [];
            foreach ($arg["value"] as $k => $v) {
                if (isset($v['teg'])) {
                    $inputs[$k] = $v;
                } else {
                    $inputs = array_merge($inputs, $v);
                }
            }
            // var_dump($inputs); 
            return $inputs;
        };
        $this->cellviews["column"] = $this->cellviews["row"];
        $this->cellviews["html"] = function($arg, $dataAnketa = []) {
            return [];
        };

        $this->cellviews["textfield"] = function($arg, $dataAnketa = []) {
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']]['value'] : $arg['default'];

            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }
            
            $reply=[
                $arg['name'] => [
                    'name' => $arg['name'],
                    'teg' => $arg['teg'],
                    'value' => ($arg['value'] ? $arg['value'] : $arg['default']),
                    'html' => ($arg['value'] ? $arg['value'] : $arg['default']),
                    'label' => $arg['label']
                ]
            ];
            // var_dump($reply); 
            return $reply;
        };

        $this->cellviews["textarea"] = $this->cellviews["textfield"];

        $this->cellviews["checkbox"] = function($arg, $dataAnketa = []) {
            $arg['checked'] = isset($dataAnketa[$this->formName][$arg['name']]['value']) ? true : false;
            if (is_bool($arg['checked'])) {
                $checked = ($arg['checked'] === true) ? 1 : 0;
            } else {
                $checked = ($arg['default'] === true) ? 1 : 0;
            }
            if (!isset($arg['value'])) {
                $arg['value'] = 1;
            }
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            return [
                $arg['name'] => [
                    'name' => $arg['name'],
                    'teg' => $arg['teg'],
                    'value' => ($checked?$arg['value']:''),
                    'html' => ($checked?$arg['value']:''),
                    'label' => $arg['label']
                ]
            ];
        };
        $this->cellviews["file"] = function($arg, $dataAnketa = []) {
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']]['value'] : $arg['default'];

            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }
            return [
                $arg['name'] => [
                    'name' => $arg['name'],
                    'teg' => $arg['teg'],
                    'value' => (  strlen($arg['value']) > 0 ? Yii::getAlias("@web/uploads/{$arg['value']}"):'' ),
                    'path' => (  strlen($arg['value']) > 0 ? $arg['value']:'' ),
                    'html' => ( strlen($arg['value']) > 0 ? ( "<a href=\"" . Yii::getAlias("@web/uploads/{$arg['value']}") . "\" target=\"_blank\">{$arg['value']}</a>" ) : ''),
                    'label' => $arg['label']
                ]
            ];
        };
        $this->cellviews["hidden"] = function($arg, $dataAnketa = []) {
            return [];
        };
        $this->cellviews["date"] = function($arg, $dataAnketa = []) {
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']]['value'] : $arg['default'];

            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }

            $timestamp = strtotime($arg['value']);
            $value=( ( $timestamp !== false) ? date(Yii::$app->params['dateFormat'], $timestamp) : '');
            return [
                $arg['name'] => [
                    'name' => $arg['name'],
                    'teg' => $arg['teg'],
                    'value' => $value,
                    'html' => $value,
                    'label' => $arg['label']
                ]
            ];
        };

        $this->cellviews["radio"] = function($arg, $dataAnketa = []) {
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']]['value'] : $arg['default'];

            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }

            $keys = [];
            $html = [];
            $value = $arg['value'] ? $arg['value'] : $arg['default'];
            if(is_array($value)){
                foreach ($arg['items'] as $key => $val) {
                    if (in_array($key, $value)) {
                        $keys[]=$key;
                        $html[]=$val;
                    }
                }
            }else{
                foreach ($arg['items'] as $key => $val) {
                    if ($key == $value) {
                        $keys[]=$key;
                        $html[]=$val;
                    }
                }
            }
            return [
                $arg['name'] => [
                    'name'   => $arg['name'],
                    'teg'    => $arg['teg'],
                    'value'  => join(', ',$keys),
                    'html'   => join(', ',$html),
                    'label'  => $arg['label']
                ]
            ];

        };
        $this->cellviews["select"] = $this->cellviews["radio"];
        
        
        $this->cellviews["tree"] = function($arg, $dataAnketa = []) {
            if (!isset($arg['default'])) {
                $arg['default'] = '';
            }
            
            $arg['value'] = isset($dataAnketa[$this->formName][$arg['name']]) ? $dataAnketa[$this->formName][$arg['name']]['value'] : $arg['default'];
            $arg['value'] = preg_replace("/^\\//","",$arg['value']);

            if (!isset($arg['label'])) {
                $arg['label'] = '';
            }

            $html = [];
                    
            $value = explode("/",$arg['value']);

            $items=$arg['items'];
            foreach($value as $val){
                if(isset($items[$val])){
                    $html[] = $items[$val]['label'];
                    if(isset($items[$val]['items']) && is_array($items[$val]['items']) ){
                        $items=$items[$val]['items'];
                    }else{
                        break;
                    }
                }else{
                    break;
                }
            }
            return [
                $arg['name'] => [
                    'name'   => $arg['name'],
                    'teg'    => $arg['teg'],
                    'value'  => $arg['value'],
                    'html'   => join(', ',$html),
                    'label'  => $arg['label']
                ]
            ];

        };
        // =================== cellview anketa = end ===========================
    }

    /*
      $obj->draw(
      [
     *     'teg'=>'row',
     *     'value'=>[
     *        [
     *          'teg'=>'column',
     *          'width'=>12,
     *          'value'=>
     *             [
     *               'teg'=>'textfield',
     *               'name'=>'email',
     *               'label'=>'Your E-Mail'
     *             ]
     *          
     *        ]
     *     ]
      ]
      );
     */

    public function form($anketa, $dataAnketa = []) {

        if (is_array($anketa) && isset($anketa['teg']) && isset($this->draw[$anketa['teg']])) {
            $keys = array_keys($anketa);

            $arg = [];
            foreach ($keys as $key) {
                if (is_array($anketa[$key])) {
                    if (isset($anketa[$key]['teg'])) {
                        $arg[$key] = $this->form($anketa[$key], $dataAnketa);
                    } else {
                        $ks = array_keys($anketa[$key]);
                        $arg[$key] = [];
                        foreach ($ks as $k) {
                            $arg[$key][$k] = $this->form($anketa[$key][$k], $dataAnketa);
                        }
                    }
                } else {
                    $arg[$key] = $anketa[$key];
                }
            }

            return $this->draw[$anketa['teg']]($arg, $dataAnketa);
        }else{
            return $anketa;
        }
    }


    public function in($anketa, $request = [], $files = []) {
        if (is_array($anketa) && isset($anketa['teg']) && isset($this->input[$anketa['teg']])) {
            $keys = array_keys($anketa);

            $arg = [];
            foreach ($keys as $key) {
                if (is_array($anketa[$key])) {
                    if (isset($anketa[$key]['teg'])) {
                        $arg[$key] = $this->in($anketa[$key], $request, $files);
                    } else {
                        $ks = array_keys($anketa[$key]);
                        $arg[$key] = [];
                        foreach ($ks as $k) {
                            $arg[$key][$k] = $this->in($anketa[$key][$k], $request, $files);
                        }
                    }
                } else {
                    $arg[$key] = $anketa[$key];
                }
            }

            return $this->input[$anketa['teg']]($arg, $request, $files);
        }else{
            return [];
        }
    }
    

    public function view($anketa, $dataAnketa = []) {
        // if (isset($this->views[$anketa['teg']])) {
        if (is_array($anketa) && isset($anketa['teg']) && isset($this->views[$anketa['teg']])) {
            $keys = array_keys($anketa);

            $arg = [];
            foreach ($keys as $key) {
                if (is_array($anketa[$key])) {
                    if (isset($anketa[$key]['teg'])) {
                        $arg[$key] = $this->view($anketa[$key], $dataAnketa);
                    } else {
                        $ks = array_keys($anketa[$key]);
                        $arg[$key] = [];
                        foreach ($ks as $k) {
                            $arg[$key][$k] = $this->view($anketa[$key][$k], $dataAnketa);
                        }
                    }
                } else {
                    $arg[$key] = $anketa[$key];
                }
            }
            return $this->views[$anketa['teg']]($arg, $dataAnketa);
        }else{
            return $anketa;
        }
    }
    
    

    public function cellview($anketa, $dataAnketa = []) {

        if (is_array($anketa) && isset($anketa['teg']) && isset($this->cellviews[$anketa['teg']])) {
            $keys = array_keys($anketa);

            $arg = [];
            foreach ($keys as $key) {
                if (is_array($anketa[$key])) {
                    if (isset($anketa[$key]['teg'])) {
                        $arg[$key] = $this->cellview($anketa[$key], $dataAnketa);
                    } else {
                        $ks = array_keys($anketa[$key]);
                        $arg[$key] = [];
                        foreach ($ks as $k) {
                            $arg[$key][$k] = $this->cellview($anketa[$key][$k], $dataAnketa);
                        }
                    }
                } else {
                    $arg[$key] = $anketa[$key];
                }
            }
            return $this->cellviews[$anketa['teg']]($arg, $dataAnketa);
        }else{
            return $anketa;
        }
    }

}
