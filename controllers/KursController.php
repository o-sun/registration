<?php

namespace app\controllers;

use Yii;
use app\models\Kurs;
use app\models\KursSearch;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\lib\ArrayHelper;

/**
 * KursController implements the CRUD actions for Kurs model.
 */
class KursController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Kurs models.
     * @return mixed
     */
    public function actionIndex() {

        if (Yii::$app->user->isGuest) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
        }

        $searchModel = new KursSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (!\app\models\User::isAdmin()) {
            $user = Yii::$app->user->identity;
            $kursIds = array_map(function($m) {
                return $m->id_kurs;
            }, $user->getKurs()->all());
            $dataProvider->query->andWhere(['id_kurs' => $kursIds]);
        }
        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Lists all public Kurs models.
     * @return mixed
     */
    public function actionPublic() {
        $searchModel = new KursSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $query = $dataProvider->query;
        $rowsPerPage = Yii::$app->params['rowsPerPage'];
        $query->andWhere(['visible' => 1]);

        $request = array_merge(Yii::$app->request->post(), Yii::$app->request->queryParams);

        $start = isset($request['start']) ? ( (int) $request['start'] ) : 0;
        return $this->render('public', [
                    'searchModel' => $searchModel,
                    'rows' => $query->limit($rowsPerPage)->offset($start)->all(),
                    'request' => $request
        ]);
    }

    /**
     * Displays a single Kurs model.
     * @param string $id
     * @return mixed
     */
    public function actionDetails($id) {
        $kurs = $this->findModel($id);
        if (!$kurs->visible) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }
        return $this->render('details', [
                    'model' => $kurs,
        ]);
    }

    /**
     * Displays a single Kurs model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        $kurs = $this->findModel($id);
        if (!$kurs->isKursManager()) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
        }
        return $this->render('view', [
                    'model' => $kurs,
        ]);
    }

    /**
     * Creates a new Kurs model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (!\app\models\User::isAdmin()) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin rights required'));
        }
        $model = new Kurs();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_kurs]);
        } else {
            $userOptions = ArrayHelper::map(User::find()->all(), 'userId', ['userPIB', 'userLogin']);
            return $this->render('create', [
                        'model' => $model,
                        'userOptions' => $userOptions,
            ]);
        }
    }

    /**
     * Updates an existing Kurs model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $kurs = $this->findModel($id);
        if (!$kurs->isKursManager()) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
        }
        if ($kurs->load(Yii::$app->request->post()) && $kurs->save()) {
            return $this->redirect(['view', 'id' => $kurs->id_kurs]);
        } else {


            $userOptions = ArrayHelper::map(User::find()->all(), 'userId', ['userPIB', 'userLogin']);
            return $this->render('update', [
                        'model' => $kurs,
                        'userOptions' => $userOptions,
            ]);
        }
    }

    /**
     * Deletes an existing Kurs model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $kurs = $this->findModel($id);
        if (!\app\models\User::isAdmin()){
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin rights required'));
        }
        $kurs->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the Kurs model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Kurs the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Kurs::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
