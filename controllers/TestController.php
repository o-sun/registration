<?php


namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class TestController extends Controller {
    /*
      $obj->draw(
      [
     *     'teg'=>'row',
     *     'value'=>[
     *        [
     *          'teg'=>'column',
     *          'width'=>12,
     *          'value'=>
     *             [
     *               'teg'=>'textfield',
     *               'name'=>'email',
     *               'label'=>'Your E-Mail'
     *             ]
     *          
     *        ]
     *     ]
      ]
      );
     */

    public function actionShowAnketa() {

        $obj = new \app\lib\ShowAnketa('anketa');

        $anketa = [
            'teg' => 'row',
            'value' => [
                [
                    'teg' => 'column',
                    'width' => 12,
                    'value' =>
                    [
                        'teg' => 'textfield',
                        'name' => 'email',
                        'label' => 'Your E-Mail'
                    ]
                ]
            ]
        ];
        //return json_encode($anketa);
        return $obj->form(
                        $anketa
        );
    }

    public function actionValidAnketa() {

        $obj = new \app\lib\ShowAnketa('anketa');

        $anketa = [
            'teg' => 'row',
            'value' => [
                [
                    'teg' => 'column',
                    'width' => 12,
                    'value' =>
                    [
                        'teg' => 'textfield',
                        'name' => 'email',
                        'label' => 'Your E-Mail',
                        'required' => true
                    ]
                ]
            ]
        ];
        $dataAnketa = [
            'anketa' => [
                'email' => ''
            ]
        ];
        //return json_encode($anketa);
        echo '<pre>';
        print_r(
                $obj->valid(
                        $anketa, $dataAnketa
        ));
        echo '</pre>';
        return 'OK';
    }

    public function actionValidAnketa1() {

        $obj = new \app\lib\ShowAnketa('anketa');

        $anketa = [

            'teg' => 'row',
            'value' => [
                [
                    'teg' => 'column',
                    'width' => 12,
                    'value' => [
                        [
                            'teg' => 'row',
                            'value' => [
                                [
                                    'teg' => 'column',
                                    'width' => 6,
                                    'value' =>
                                    [
                                        'teg' => 'textfield',
                                        'name' => 'email',
                                        'label' => 'Your E-Mail',
                                        'required' => true
                                    ]
                                ],
                                [
                                    'teg' => 'column',
                                    'width' => 6,
                                    'value' =>
                                    [
                                        'teg' => 'textarea',
                                        'name' => 'etxtarea',
                                        'label' => 'fff',
                                        'value' => 'Your E-Mailgvfnmkhj;gkn 
                                                         gkjhgnh;l kghnjgflhnjg',
                                        'required' => true
                                    ]
                                ]
                            ]
                        ]
                        ,
                        [
                            'teg' => 'row',
                            'value' => [
                                [
                                    'teg' => 'column',
                                    'width' => 4,
                                    'value' =>
                                    [
                                        'teg' => 'checkbox',
                                        'name' => 'checkbox1',
                                        'label' => 'checkbox 1',
                                        'value' => 'check1',
                                        'checked' => true,
                                        'required' => true
                                    ]
                                ],
                                [
                                    'teg' => 'column',
                                    'width' => 3,
                                    'value' =>
                                    [
                                        'teg' => 'checkbox',
                                        'name' => 'checkbox2',
                                        'label' => 'checkbox 2',
                                        'value' => 'check2',
                                    ]
                                ],
                                [
                                    'teg' => 'column',
                                    'width' => 3,
                                    'value' =>
                                    [
                                        'teg' => 'file',
                                        'name' => 'file1',
                                        'label' => 'file ffff',
                                        'tip' => 'fgh fgh ffhj j',
                                        'required' => true
                                    ]
                                ]
                            ]
                        ]
                        ,
                        [
                            'teg' => 'row',
                            'value' => [
                                [
                                    'teg' => 'column',
                                    'width' => 3,
                                    'value' =>
                                    [
                                        'teg' => 'radio',
                                        'name' => 'radio8',
                                        'label' => 'fghfkkhg',
                                        'items' => ['1' => 'r1',
                                            '2' => 'r2'],
                                        'value' => '1',
                                        'required' => true
                                    ]
                                ],
                                [
                                    'teg' => 'column',
                                    'width' => 3,
                                    'value' =>
                                    [
                                        'teg' => 'radio',
                                        'name' => 'radio1',
                                        'label' => 'fghfk dfvd dvf khg',
                                        'items' => ['1' => 'r1',
                                            '2' => 'r2'],
                                        'default' => '2'
                                    ]
                                ],
                                [
                                    'teg' => 'column',
                                    'width' => 3,
                                    'value' =>
                                    [
                                        'teg' => 'radio',
                                        'name' => 'radio2',
                                        'label' => 'fghfk vkhg',
                                        'items' => ['1' => 'r1',
                                            '2' => 'r2'],
                                    ]
                                ],
                                [
                                    'teg' => 'column',
                                    'width' => 3,
                                    'value' =>
                                    [
                                        'teg' => 'select',
                                        'name' => 'select1',
                                        'label' => 'qwert fghfkkhg',
                                        'items' => ['1' => 'r1',
                                            '2' => 'r2'],
                                        'required' => true
                                    ]
                                ]
                            ]
                        ]
                        ,
                        [
                            'teg' => 'row',
                            'value' => [
                                [
                                    'teg' => 'column',
                                    'width' => 4,
                                    'value' =>
                                    [
                                        'teg' => 'hidden',
                                        'name' => 'hidden1',
                                        'value' => 'hidden1 vld1',
                                    ]
                                ],
                                [
                                    'teg' => 'column',
                                    'width' => 3,
                                    'value' =>
                                    [
                                        'teg' => 'date',
                                        'name' => 'date2',
                                        'label' => 'date 2',
                                        'value' => '10.11.15',
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];

      /*  $dataAnketa = [
            'anketa' => [
                'email' => 'rtr@drt.ty',
                'etxtarea' => '',
                'checkbox1' => '',
                'checkbox2' => 'nmk nkj',
                'file1' => '',
                'radio8' => '',
                'radio1' => '1',
                'radio2' => '3',
                'select1' => '',
                'date2' => '10.11 15'
            ]
        */
          $dataAnketa = [
            'anketa' => [
                'email' => 'rtr@drt.ty',
                'etxtarea' => 'vhkjdf bhjfk ',
                'checkbox1' => 'fd',
                'checkbox2' => 'nmk nkj',
                'file1' => 'hjk bk',
                'radio8' => '2',
                'radio1' => '1',
                'radio2' => '2',
                'select1' => '1',
                'date2' => '10.11.15'
            ]   
        ];
        //return json_encode($anketa);
          
          
        $in = $obj->in($anketa, $dataAnketa, [] );
        echo '<pre>';
        print_r($in);
        echo '</pre>';
        echo '<pre>';
        print_r(array_reduce(array_map(function($e){return count($e['errors']);},$in),function($v,$w){return $v+$w;}));
        echo '</pre>';
        return 'OK';
    }

    public function actionShowAnketa1() {

        $obj = new \app\lib\ShowAnketa('a');

        $anketa = [

            'teg' => 'row',
            'value' => [
                [
                    'teg' => 'column',
                    'width' => 12,
                    'value' => [
                        [
                            'teg' => 'row',
                            'value' => [
                                [
                                    'teg' => 'column',
                                    'width' => 6,
                                    'value' =>
                                    [
                                        'teg' => 'textfield',
                                        'name' => 'email',
                                        'label' => 'Your E-Mail'
                                    ]
                                ],
                                [
                                    'teg' => 'column',
                                    'width' => 6,
                                    'value' =>
                                    [
                                        'teg' => 'textarea',
                                        'name' => 'etxtarea',
                                        'label' => 'fff',
                                        'value' => 'Your E-Mailgvfnmkhj;gkn 
                                                         gkjhgnh;l kghnjgflhnjg'
                                    ]
                                ]
                            ]
                        ]
                        ,
                        [
                            'teg' => 'row',
                            'value' => [
                                [
                                    'teg' => 'column',
                                    'width' => 4,
                                    'value' =>
                                    [
                                        'teg' => 'checkbox',
                                        'name' => 'checkbox1',
                                        'label' => 'checkbox 1',
                                        'value' => 'check1',
                                        'checked' => true
                                    ]
                                ],
                                [
                                    'teg' => 'column',
                                    'width' => 3,
                                    'value' =>
                                    [
                                        'teg' => 'checkbox',
                                        'name' => 'checkbox2',
                                        'label' => 'checkbox 2',
                                        'value' => 'check2',
                                    ]
                                ],
                                [
                                    'teg' => 'column',
                                    'width' => 3,
                                    'value' =>
                                    [
                                        'teg' => 'file',
                                        'name' => 'file1',
                                        'label' => 'file ffff',
                                        'tip' => 'fgh fgh ffhj j'
                                    ]
                                ]
                            ]
                        ]
                        ,
                        [
                            'teg' => 'row',
                            'value' => [
                                [
                                    'teg' => 'column',
                                    'width' => 3,
                                    'value' =>
                                    [
                                        'teg' => 'radio',
                                        'name' => 'radio8',
                                        'label' => 'fghfkkhg',
                                        'items' => ['1' => 'r1',
                                            '2' => 'r2'],
                                        'value' => '1'
                                    ]
                                ],
                                [
                                    'teg' => 'column',
                                    'width' => 3,
                                    'value' =>
                                    [
                                        'teg' => 'radio',
                                        'name' => 'radio1',
                                        'label' => 'fghfk dfvd dvf khg',
                                        'items' => ['1' => 'r1',
                                            '2' => 'r2'],
                                        'default' => '2'
                                    ]
                                ],
                                [
                                    'teg' => 'column',
                                    'width' => 3,
                                    'value' =>
                                    [
                                        'teg' => 'radio',
                                        'name' => 'radio2',
                                        'label' => 'fghfk vkhg',
                                        'items' => ['1' => 'r1',
                                            '2' => 'r2'],
                                    ]
                                ],
                                [
                                    'teg' => 'column',
                                    'width' => 3,
                                    'value' =>
                                    [
                                        'teg' => 'select',
                                        'name' => 'select1',
                                        'label' => 'qwert fghfkkhg',
                                        'items' => ['1' => 'r1',
                                            '2' => 'r2'],
                                    ]
                                ]
                            ]
                        ]
                        ,
                        [
                            'teg' => 'row',
                            'value' => [
                                [
                                    'teg' => 'column',
                                    'width' => 4,
                                    'value' =>
                                    [
                                        'teg' => 'hidden',
                                        'name' => 'hidden1',
                                        'value' => 'hidden1 vld1',
                                    ]
                                ],
                                [
                                    'teg' => 'column',
                                    'width' => 3,
                                    'value' =>
                                    [
                                        'teg' => 'date',
                                        'name' => 'date2',
                                        'label' => 'date 2',
                                        'value' => '10.11.15',
                                    ]
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        return $obj->form($anketa);
    }

}
