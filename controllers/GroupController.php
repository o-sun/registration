<?php

namespace app\controllers;

use Yii;
use app\models\Group;
use app\models\Kurs;
use app\models\GroupSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * GroupController implements the CRUD actions for Group model.
 */
class GroupController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Group models.
     * @return mixed
     */
    public function actionIndex($id_kurs) {
        $kurs = Kurs::findOne($id_kurs);

        if (!$kurs) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }

        if(!$kurs->isKursManager()){
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
        }
        
        $searchModel = new GroupSearch($id_kurs);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
                    'searchModel' => $searchModel, 'kurs' => $kurs,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Group model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        $group=$this->findModel($id);
        $kurs=$group->idKurs;
        if(!$kurs->isKursManager()){
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
        }

        return $this->render('view', [
            'model' => $group,
        ]);
    }

    /**
     * Creates a new Group model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_kurs) {
        $kurs = Kurs::findOne($id_kurs);

        if (!$kurs) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }

        if(!$kurs->isKursManager()){
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
        }
        $model = new Group();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_group]);
        } else {

            $kursOptions = ArrayHelper::map(Kurs::find()->all(), 'id_kurs', 'name_kurs');

            return $this->render('create', [
                        'model' => $model,
                        'kursOptions' => $kursOptions,
                        'kurs' => $kurs
            ]);
        }
    }

    /**
     * Updates an existing Group model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $group = $this->findModel($id);
        $kurs=$group->idKurs;
        if(!$kurs->isKursManager()){
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
        }

        if ($group->load(Yii::$app->request->post()) && $group->save()) {
            return $this->redirect(['view', 'id' => $group->id_group]);
        } else {
            $kursOptions = ArrayHelper::map(Kurs::find()->all(), 'id_kurs', 'name_kurs');

            return $this->render('update', [
                        'model' => $group,
                        'kursOptions' => $kursOptions
            ]);
        }
    }

    /**
     * Deletes an existing Group model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $group = $this->findModel($id);
        $kurs=$group->idKurs;
        if(!$kurs->isKursManager()){
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
        }
        $group->delete();
        return $this->redirect(['index', 'id_kurs'=>$kurs->id_kurs]);
    }

    /**
     * Finds the Group model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Group the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Group::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
