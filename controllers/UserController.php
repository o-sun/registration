<?php

namespace app\controllers;

use Yii;
use app\models\User;
use app\models\Kurs;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\lib\ArrayHelper;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex() {
        if (!\app\models\User::isAdmin()) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin rights required'));
        }
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        if (!\app\models\User::isAdmin()) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin rights required'));
        }
        return $this->render('view', [
                    'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        if (!\app\models\User::isAdmin()) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin rights required'));
        }
        $model = new User();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if (isset($post['userPassword1']) && isset($post['userPassword2']) && strlen($post['userPassword1']) > 0 && $post['userPassword1'] == $post['userPassword2']) {
                $model->setPassword($post['userPassword1']);
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->userId]);
        } else {
            $kurses = ArrayHelper::map(Kurs::find()->all(), 'id_kurs', ['name_kurs']);
            return $this->render('create', [
                        'model' => $model,
                        'kurses' => $kurses,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        if (!\app\models\User::isAdmin()) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin rights required'));
        }
        $model = $this->findModel($id);

        $post = Yii::$app->request->post();
        if ($model->load($post) && $model->save()) {
            if (isset($post['userPassword1']) && isset($post['userPassword2']) && strlen($post['userPassword1']) > 0 && $post['userPassword1'] == $post['userPassword2']) {
                $model->setPassword($post['userPassword1']);
                $model->save();
            }
            return $this->redirect(['view', 'id' => $model->userId]);
        } else {
            $kurses = ArrayHelper::map(Kurs::find()->all(), 'id_kurs', ['name_kurs']);
            return $this->render('update', [
                        'model' => $model,
                        'kurses' => $kurses,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id) {
        if (!\app\models\User::isAdmin()) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin rights required'));
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
