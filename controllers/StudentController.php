<?php

namespace app\controllers;

use Yii;
use app\models\Group;
use app\models\Student;
use app\models\StudentSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;
use app\lib\ArrayHelper;

/**
 * StudentController implements the CRUD actions for Student model.
 */
class StudentController extends Controller {

    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Student models.
     * @return mixed
     */
    public function actionIndex($id_group) {
        $group = Group::findOne($id_group);
        if (!$group) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }
        $kurs = $group->idKurs;
        if (!$kurs->isKursManager()) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
        }

        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andFilterWhere([ 'id_group' => $id_group,]);

        return $this->render('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                    'group' => $group,
                    'kurs' => $kurs
        ]);
    }

    public function actionDownload($id_group) {

        $group = Group::findOne($id_group);
        if (!$group) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }
        $kurs = $group->idKurs;
        if (!$kurs->isKursManager()) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
        }

        $dir = Yii::getAlias("@webroot/uploads/kurs{$kurs->id_kurs}");
        if (!file_exists($dir)) {
            mkdir($dir);
        }

        $destination = "{$dir}/group{$group->id_group}.zip";
        if (file_exists($destination)) {
            unlink($destination);
        }

        // create list
        $searchModel = new StudentSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $query = $dataProvider->query;
        $query->andFilterWhere([ 'id_group' => $id_group,]);
        $students = $query->all();

        $table = "{$dir}/group{$group->id_group}.html";
        if (file_exists($table)) {
            unlink($table);
        }
        $csv = "{$dir}/group{$group->id_group}.csv";
        if (file_exists($csv)) {
            unlink($csv);
        }
        $files = [];
        $rows = [];
        $colNames = [];

        
        $colNames['id_stud'] = Yii::$app->lang->t('id_stud');
        $colNames['firstName'] = Yii::$app->lang->t('firstName');
        $colNames['middleName'] = Yii::$app->lang->t('middleName');
        $colNames['lastName'] = Yii::$app->lang->t('lastName');
        $colNames['email'] = 'email';
        
        
        $blank = $group->blankAnkety();
        $anketa = new \app\lib\ShowAnketa('anketa');
        $postedAnketaData = $anketa->in($blank, [], []);
        foreach($postedAnketaData as $elm){
            if (!isset($colNames[$elm['name']])) {
                $colNames[$elm['name']] = $elm['label'];
            }
        }
        
        foreach ($students as $student) {
            $colValues = [];
            $colValues['id_stud'] = $student->id_stud;
            $colValues['firstName'] = $student->firstName;
            $colValues['middleName'] = $student->middleName;
            $colValues['lastName'] = $student->lastName;
            $colValues['email'] = $student->email;

            $anketaData = json_decode($student->anketa, true);
            // var_dump($anketaData); exit('1');
            if ($anketaData) {
                
                $cellviews=$anketa->cellview($blank, ['anketa' => $anketaData]);
                // var_dump($cellviews); exit('1');
                
                foreach ($cellviews as $elm) {
                    // var_dump($elm); // continue;
                    if(!isset($elm['name'])){
                        continue;
                    }
                    if ($elm['teg'] == 'file') {
                        $colValues[$elm['name']] = '<a href="' . basename($elm['value']) . '">' . basename($elm['value']) . '</a>';
                        $files[] = $elm['path'];
                    } else {
                        $colValues[$elm['name']] = $elm['html'];
                    }
                }
            }
            // exit('2');
            // var_dump($colValues); exit('1');
            $rows[] = $colValues;
        }


        
        
        // ============= create html = begin ===================================
        $html = "<!DOCTYPE html>
            <html>
            <head><meta charset=\"utf-8\"></head>
            <body>
            <h1>{$kurs->name_kurs}</h1>
            <h2>{$group->name_group} {$group->date_start}</h2>
            <table>
            ";

        $html.="<thead><tr>";
        foreach ($colNames as $colName) {
            $html.="<th>" . htmlspecialchars($colName) . "</th>";
        }
        $html.="</tr></thead>";

        $html.="<tbody>";
        foreach ($rows as $row) {
            $html.="<tr>";
            foreach ($colNames as $colKey => $colName) {
                $html.="<td>" . (isset($row[$colKey]) ? $row[$colKey] : '') . "</td>";
            }
            $html.="</tr>";
        }
        $html.="</tbody>";
        $html.="</table></body></html>";
        file_put_contents($table, $html);
        // ============= create html = end =====================================
        
        //        
        //        // ============= create csv = begin ====================================
        //        $fp = fopen($csv, 'w');
        //
        //        fputcsv($fp, $colNames);
        //        
        //        foreach ($rows as $row) {
        //            $csvrow=[];
        //            foreach ($colNames as $colKey => $colName) {
        //                $val=(isset($row[$colKey]) ? $row[$colKey] : '');
        //                if(preg_match("/^<a /",$val)){
        //                    $csvrow[]='=HYPERLINK("'.strip_tags($val).'")';
        //                }else{
        //                    $csvrow[]= strip_tags($val);
        //                }
        //            }
        //            fputcsv($fp, $csvrow);
        //        }
        //
        //        fclose($fp);
        //
        //        // ============= create csv = end ======================================

        $zip = new \ZipArchive();
        
        $prefix="group{$group->id_group}/";

        if ($zip->open($destination, \ZIPARCHIVE::CREATE) !== true) {
            return false;
        }

        $zip->addFile($table, $prefix.basename($table));
        //$zip->addFile($csv);
        foreach ($files as $thefile) {
            $f=Yii::getAlias("@webroot/uploads/{$thefile}");
            if(is_file($f) && file_exists($f)){
                $zip->addFile($f, $prefix.basename($f));
            }
        }
        $zip->close();

        $url = Yii::getAlias("@web/uploads/kurs{$kurs->id_kurs}/group{$group->id_group}.zip");

        //header('Content-Description: File Transfer');
        //header('Content-Type: application/octet-stream');
        //header('Content-Transfer-Encoding: binary');
        //header('Content-Disposition: attachment; filename=' . basename($destination));
        //header('Expires: 0');
        //header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        //header('Pragma: public');
        // header('Content-Length: ' . filesize($destination));
        header('Location:' . $url);
        exit();
        //return $url;
    }

    public function actionReminder($id_group) {
        // find group
        $group = Group::findOne($id_group);
        if (!$group || !$group->visible) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }
        if ($group->isExpired()) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }
        $kurs = $group->idKurs;
        if (!$kurs || !$kurs->visible) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }


        $message = '';
        $post = Yii::$app->request->post();
        if (isset($post['email'])) {
            // find student by email
            $student = Student::find()->where(['id_group' => $id_group, 'email' => $post['email']])->one();
            if ($student) {
                // create new secret code
                $student->secret_code = Yii::$app->security->generateRandomString(48);
                $student->save();
                // mail the secret URL
                $kurs = $group->idKurs;
                Yii::$app->mailer->compose(
                                '@app/mail/registration/reminder.php', [
                            'group' => $group,
                            'student' => $student,
                            'kurs' => $kurs
                        ])
                        ->setFrom(Yii::$app->params['adminEmail'])
                        ->setTo($student->email)
                        ->setSubject(Yii::$app->lang->t("{siteName}: registration to {name_kurs}",['siteName'=>Yii::$app->params['siteName'],'name_kurs'=>$kurs->name_kurs]) )
                        ->send();
                $message = Yii::$app->lang->t("Secure link posted to {email}",['email'=>$post['email']]);
                return $this->render('reminder-success', [
                    'group' => $group,
                    'kurs' => $group->idKurs,
                    'message' => $message
                ]);
            } else {
                $message = Yii::$app->lang->t('Email {email} not found',['email'=>$post['email']]);
            }
        }

        return $this->render('reminder', [
                    'group' => $group,
                    'kurs' => $group->idKurs,
                    'message' => $message
        ]);
    }

    /**
     * Displays anketa.
     * @param string $id
     * @return mixed
     */
    public function actionRegistration($id_group) {

        // find group
        $group = Group::findOne($id_group);
        if (!$group || !$group->visible) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }
        if ($group->isExpired()) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }
        $kurs = $group->idKurs;
        if (!$kurs || !$kurs->visible) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }

        $blank = $group->blankAnkety();

        $student = new Student();

        $post = Yii::$app->request->post();

        $errors = '';

        $anketa = new \app\lib\ShowAnketa('anketa');


        // echo "<pre>";        print_r($post);        echo '</pre>';

        $postedAnketaData=[];
        if (isset($post['anketa'])) {

            $studentDataLoaded = $student->load($post);

            //$postedAnketaData = $anketa->in($blank, $post, Yii::$app->request->post());
            $postedAnketaData = $anketa->in($blank, $post, $_FILES);
            // echo "<br><br><br><br><pre> input data = \n";       print_r($in);          echo "</pre>";

            $numberOfErrors = array_reduce(array_map(function($e) {
                        return count($e['errors']);
                    }, $postedAnketaData), function($v, $w) {
                return $v + $w;
            });
            // echo "<pre>";           print_r($numberOfErrors);           echo '</pre>';
            //id_group
            //Fio
            //email 

            // check if email is unique
            $otherStudentFound=false;
            //if(\Yii::$app->params['allow_corrections']){
            $otherStudent = Student::find()->where(['id_group' => $id_group, 'email' => $post['Student']['email']])->one();
            if ($otherStudent) {
                $otherStudentFound=true;
                $student->addError('email', Yii::$app->lang->t('Email {email} already taken',['email'=>$post['Student']['email']]));
            }
            //}
            
            if ($numberOfErrors == 0 && $studentDataLoaded && !$otherStudentFound) {
                $student->id_group = $id_group;
                $student->secret_code = Yii::$app->security->generateRandomString(48);
                $student->dateCreated = date('Y-m-d');
                $student->save();
                // upload files
                foreach ($postedAnketaData as &$input) {
                    if ($input['teg'] == 'file') {
                        $file = UploadedFile::getInstanceByName("anketa[{$input['name']}]");
                        if ($file) {
                            $dir = Yii::getAlias("@webroot/uploads/kurs{$group->id_kurs}");
                            if (!file_exists($dir)) {
                                mkdir($dir);
                            }
                            $name = "kurs{$group->id_kurs}_group{$id_group}_stud{$student->id_stud}_{$input['name']}." . $file->getExtension();
                            $file->saveAs("{$dir}/{$name}");
                            $input['value'] = "kurs{$group->id_kurs}/{$name}";
                        }
                    }
                }
                // save anketa
                // echo "<br><br><br><br><pre> input data = \n";  print_r($in);  echo "</pre>";
                $student->anketa = json_encode($postedAnketaData);


                // collect keywords
                $keyWords="";
                
                
                $student->keyWords=$student->getKeyWords();
                
                $student->save();

                // notify student
                $kurs = $group->getIdKurs()->one();
                Yii::$app->mailer->compose(
                                '@app/mail/registration/confirmation.php', [
                            'group' => $group,
                            'student' => $student,
                            'kurs' => $kurs
                        ])
                        ->setFrom(Yii::$app->params['adminEmail'])
                        ->setTo($student->email)
                        ->setSubject(Yii::$app->lang->t("{siteName}: registration to {name_kurs}",['siteName'=>Yii::$app->params['siteName'],'name_kurs'=>$kurs->name_kurs]))
                        ->send();
                // show results
                return $this->redirect(['view-registration', 'id_stud' => $student->id_stud, 'secret_code'=>$student->secret_code]);
            } else {
                // show errors                
            }
        }

        return $this->render('registration', [
                    'group' => $group,
                    'student' => $student,
                    'post' => Yii::$app->request->post(),
                    'postedAnketaData'=>$postedAnketaData
        ]);
    }

    public function actionUpdateRegistration($id_stud, $secret_code) {
        $student = Student::find()->where(['id_stud' => $id_stud, 'secret_code' => $secret_code])->one();
        if (!$student) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }

        $group = $student->idGroup;
        if (!$group || !$group->visible) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }
        if (!$group->visible) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Registration finished'));
        }
        if ($group->isExpired()) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Registration finished'));
        }
        $kurs = $group->idKurs;
        if (!$kurs || !$kurs->visible) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Registration finished'));
        }



        // get saved anketa
        $anketaData = json_decode($student->anketa, true);

        //echo '<br><br><br><br>';
        //var_dump($anketaData);
        $post = Yii::$app->request->post();

        unset($post['Student']['email']);
        if (isset($post['anketa'])) {
            $basicDataLoaded = $student->load($post);

            // get posted anketa
            $blank = $group->blankAnkety();
            $anketaParser = new \app\lib\ShowAnketa('anketa');
            $anketaPostedData = $anketaParser->in($blank, $post);
            // var_dump($anketaPostedData);
            // override saved anketa
            //   if file
            //      if file posted
            //         remove old file
            //         upload new file
            //         set new value
            //      else
            //         if old file exists 
            //            remove error messages
            //   else
            //     override old value
            $dir = Yii::getAlias("@webroot/uploads/kurs{$group->id_kurs}");
            if (!file_exists($dir)) {
                mkdir($dir);
            }
            foreach ($anketaPostedData as $name => $input) {
                $anketaData[$name]['errors'] = $input['errors'];

                if ($input['teg'] == 'file') {
                    $file = UploadedFile::getInstanceByName("anketa[{$input['name']}]");
                    if ($file) {
                        $fname = "kurs{$group->id_kurs}_group{$group->id_group}_stud{$student->id_stud}_{$input['name']}." . $file->getExtension();
                        $file->saveAs("{$dir}/{$fname}");
                        $anketaData[$name]['value'] = "kurs{$group->id_kurs}/{$fname}";
                    } else {
                        if ($anketaData[$name]['value']) {
                            $anketaData[$name]['errors'] = [];
                        }
                    }
                } else {
                    $anketaData[$name]['value'] = $input['value'];
                }
            }

            // count errors
            // for each posted anketa item
            //     if required and value is empty
            //        $anketaDataCorrect=false;
            $numberOfErrors = array_reduce(array_map(function($e) {
                        return count($e['errors']);
                    }, $anketaData), function($v, $w) {
                return $v + $w;
            });

            if ($basicDataLoaded && ($numberOfErrors == 0) && $student->save()) {
                $student->anketa = json_encode($anketaData);
                $student->keyWords=$student->getKeyWords();
                $student->save();
                return $this->redirect(['view-registration', 'id_stud' => $student->id_stud, 'secret_code'=>$student->secret_code]);
            }
        }






        $groupOptions = ArrayHelper::map(Group::find()->where(['id_kurs' => $group->id_kurs])->all(), 'id_group', ['name_group', 'date_start']);
        return $this->render('update-registration', [
                    'student' => $student,
                    'groupOptions' => $groupOptions,
                    'group' => $group,
                    'kurs' => $kurs,
                    'anketaData' => $anketaData
        ]);
    }

    
    
    public function actionDeleteRegistration($id_stud, $secret_code) {
        $student = Student::find()->where(['id_stud' => $id_stud, 'secret_code' => $secret_code])->one();
        if (!$student) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }

        $group = $student->idGroup;
        if (!$group || !$group->visible) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }
        if (!$group->visible) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Registration finished'));
        }
        if ($group->isExpired()) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Registration finished'));
        }
        $kurs = $group->idKurs;
        if (!$kurs || !$kurs->visible) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Registration finished'));
        }



        $post = Yii::$app->request->post();
        $anketaData = json_decode($student->anketa, true);
        if(isset($post['del'])){
            // delete uploaded files
            $anketaData = json_decode($student->anketa, true);
            if ($anketaData) {
                foreach ($anketaData as $v) {
                    if ($v['teg'] == 'file' && strlen(trim($v['value'])) > 0) {
                        $f = Yii::getAlias("@webroot/uploads/{$v['value']}");
                        unlink($f);
                    }
                }
            }

            $student->delete();
            return $this->render('delete-registration-ok', [
                        'student' => $student,
                        'group' => $group,
                        'kurs' => $kurs,
                        'anketaData' => $anketaData
            ]);
        }else{
            return $this->render('delete-registration', [
                        'student' => $student,
                        'group' => $group,
                        'kurs' => $kurs,
                        'anketaData' => $anketaData
            ]);
        }

    }
    
    
    
    
    
    public function actionViewRegistration($id_stud, $secret_code) {

        $student = Student::find()->where(['id_stud' => $id_stud])->one();
        if (!$student) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }
        $group = $student->idGroup;
        $kurs = $group->idKurs;
        if (!$kurs->isKursManager() && $secret_code != $student->secret_code) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }

        $group = $student->idGroup;
        $kurs = $group->idKurs;
        return $this->render('view-registration', [
                    'model' => $student,
                    'group' => $group,
                    'kurs' => $kurs,
        ]);
    }

    /**
     * Displays a single Student model.
     * @param string $id
     * @return mixed
     */
    public function actionView($id) {
        $student = $this->findModel($id);
        $group = $student->idGroup;
        $kurs = $group->idKurs;
        if (!$kurs->isKursManager()) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
        }
        return $this->render('view', [
                    'model' => $student,
                    'group' => $group,
                    'kurs' => $kurs,
        ]);
    }

    /**
     * Creates a new Student model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($id_group) {
        $student = new Student();

        $group = Group::findOne($id_group);
        if (!$group) {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }
        $kurs = $group->idKurs;
        if (!$kurs->isKursManager()) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
        }

        if ($student->load(Yii::$app->request->post()) && $student->save()) {
            // TODO update anketa
            return $this->redirect(['view', 'id' => $student->id_stud]);
        } else {
            $student->id_group = $id_group;
            $groupOptions = ArrayHelper::map(Group::find()->where(['id_kurs' => $group->id_kurs])->all(), 'id_group', ['name_group', 'date_start']);
            return $this->render('create', [
                        'model' => $student,
                        'groupOptions' => $groupOptions,
                        'group' => $group,
                        'kurs' => $kurs
            ]);
        }
    }

    /**
     * Updates an existing Student model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $student = $this->findModel($id);
        $group = $student->idGroup;
        $kurs = $group->idKurs;
        if (!$kurs->isKursManager()) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
        }

        // get saved anketa
        $anketaData = json_decode($student->anketa, true);

        //echo '<br><br><br><br>';
        //var_dump($anketaData);
        $post = Yii::$app->request->post();
        if (isset($post['anketa'])) {
            
            $statusOld=$student->status;
            
            $basicDataLoaded = $student->load($post);

            // get posted anketa
            $blank = $group->blankAnkety();
            $anketaParser = new \app\lib\ShowAnketa('anketa');
            $anketaPostedData = $anketaParser->in($blank, $post);
            // var_dump($anketaPostedData);
            // override saved anketa
            //   if file
            //      if file posted
            //         remove old file
            //         upload new file
            //         set new value
            //      else
            //         if old file exists 
            //            remove error messages
            //   else
            //     override old value
            $dir = Yii::getAlias("@webroot/uploads/kurs{$group->id_kurs}");
            if (!file_exists($dir)) {
                mkdir($dir);
            }
            foreach ($anketaPostedData as $name => $input) {
                $anketaData[$name]['errors'] = $input['errors'];

                if ($input['teg'] == 'file') {
                    $file = UploadedFile::getInstanceByName("anketa[{$input['name']}]");
                    if ($file) {
                        $fname = "kurs{$group->id_kurs}_group{$group->id_group}_stud{$student->id_stud}_{$input['name']}." . $file->getExtension();
                        $file->saveAs("{$dir}/{$fname}");
                        $anketaData[$name]['value'] = "kurs{$group->id_kurs}/{$fname}";
                    } else {
                        if ($anketaData[$name]['value']) {
                            $anketaData[$name]['errors'] = [];
                        }
                    }
                } else {
                    $anketaData[$name]['value'] = $input['value'];
                }
            }

            // count errors
            // for each posted anketa item
            //     if required and value is empty
            //        $anketaDataCorrect=false;
            $numberOfErrors = array_reduce(array_map(function($e) {
                        return count($e['errors']);
                    }, $anketaData), function($v, $w) {
                return $v + $w;
            });

            if ($basicDataLoaded && ($numberOfErrors == 0) && $student->save()) {
                $student->anketa = json_encode($anketaData);
                $student->keyWords=$student->getKeyWords();
                $student->save();
                
                if($statusOld!=$student->status){
                    Yii::$app->mailer->compose(
                                '@app/mail/registration/statusupdate.php', [
                                'group' => $group,
                                'student' => $student,
                                'kurs' => $kurs
                            ])
                            ->setFrom(Yii::$app->params['adminEmail'])
                            ->setTo($student->email)
                            ->setSubject(Yii::$app->lang->t("{siteName}: registration status updated {name_kurs}",['siteName'=>Yii::$app->params['siteName'],'name_kurs'=>$kurs->name_kurs]))
                            ->send();

                }
                
                
                
                
                return $this->redirect(['view', 'id' => $student->id_stud]);
            }
        }
        $groupOptions = ArrayHelper::map(Group::find()->where(['id_kurs' => $group->id_kurs])->all(), 'id_group', ['name_group', 'date_start']);
        return $this->render('update', [
                    'student' => $student,
                    'groupOptions' => $groupOptions,
                    'group' => $group,
                    'kurs' => $kurs,
                    'anketaData' => $anketaData
        ]);
    }

    
    public function actionSetstatus(){
        $this->enableCsrfValidation = false;
        $post = Yii::$app->request->post();
        $newStatus=$post['status'];
        $statusOptions = Student::getStatusOptions();
        if(strlen($newStatus)==0 || !isset($statusOptions[$newStatus])){
            return '{"status":"error"}';
        }
        $student = $this->findModel($post['id_stud']);
        if($student){
            $group = $student->idGroup;
            $kurs = $group->idKurs;
            if (!$kurs->isKursManager()) {
                throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
            }
            $student->status=$newStatus;
            $student->save();

            Yii::$app->mailer->compose(
                        '@app/mail/registration/statusupdate.php', [
                        'group' => $group,
                        'student' => $student,
                        'kurs' => $kurs
                    ])
                    ->setFrom(Yii::$app->params['adminEmail'])
                    ->setTo($student->email)
                    ->setSubject(Yii::$app->lang->t("{siteName}: registration status updated {name_kurs}",['siteName'=>Yii::$app->params['siteName'],'name_kurs'=>$kurs->name_kurs]))
                    ->send();
            
        }
        return '{"status":"success"}';
    }
    
    /**
     * Deletes an existing Student model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     */
    public function actionDelete($id) {
        $student = $this->findModel($id);

        $group = $student->idGroup;
        $kurs = $group->idKurs;
        if (!$kurs->isKursManager()) {
            throw new \yii\web\ForbiddenHttpException(Yii::$app->lang->t('Admin/manager rights required'));
        }

        // delete uploaded files
        $anketaData = json_decode($student->anketa, true);
        if ($anketaData) {
            foreach ($anketaData as $v) {
                if (isset($v['teg']) && $v['teg'] == 'file' && strlen(trim($v['value'])) > 0) {
                    $f = Yii::getAlias("@webroot/uploads/{$v['value']}");
                    if(is_file($f) && file_exists($f)){
                        unlink($f);
                    }
                }
            }
        }

        $student->delete();

        return $this->redirect(['index', 'id_group' => $group->id_group]);
    }

    /**
     * Finds the Student model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Student the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id) {
        if (($model = Student::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::$app->lang->t('Page not found'));
        }
    }

}
