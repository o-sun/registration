/*
SQLyog Community v12.12 (32 bit)
MySQL - 5.5.47-0+deb7u1-log : Database - regisration
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `group` */

CREATE TABLE `group` (
  `id_group` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_kurs` bigint(20) DEFAULT NULL,
  `name_group` varchar(255) DEFAULT NULL,
  `date_start` date DEFAULT NULL,
  `max_count_stud` int(11) DEFAULT NULL,
  `visible` bit(1) DEFAULT NULL,
  `anketa` blob,
  PRIMARY KEY (`id_group`),
  KEY `id_kurs` (`id_kurs`),
  CONSTRAINT `group_ibfk_1` FOREIGN KEY (`id_kurs`) REFERENCES `kurs` (`id_kurs`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

/*Data for the table `group` */

LOCK TABLES `group` WRITE;

insert  into `group`(`id_group`,`id_kurs`,`name_group`,`date_start`,`max_count_stud`,`visible`,`anketa`) values (1,1,'gfbg','2016-11-15',25,'','');
insert  into `group`(`id_group`,`id_kurs`,`name_group`,`date_start`,`max_count_stud`,`visible`,`anketa`) values (2,1,'ккк','2016-11-16',12,'','');

UNLOCK TABLES;

/*Table structure for table `kurs` */

CREATE TABLE `kurs` (
  `id_kurs` bigint(20) NOT NULL AUTO_INCREMENT,
  `name_kurs` varchar(255) NOT NULL,
  `description` text,
  `anketa` blob,
  `visible` bit(1) DEFAULT NULL,
  PRIMARY KEY (`id_kurs`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Data for the table `kurs` */

LOCK TABLES `kurs` WRITE;

insert  into `kurs`(`id_kurs`,`name_kurs`,`description`,`anketa`,`visible`) values (1,'testgghh','descr','{\"teg\":\"row\",\"value\":[\r\n{\"teg\":\"column\",\"width\":12,\"value\":[\r\n{\"teg\":\"textfield\",\"name\":\"email\",\"label\":\"Your E-Mail\"},\r\n{\"teg\":\"file\",\"name\":\"f1\",\"label\":\"load file hhjfkldhf\"}\r\n]}]}','');

UNLOCK TABLES;

/*Table structure for table `student` */

CREATE TABLE `student` (
  `id_stud` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_group` bigint(20) DEFAULT NULL,
  `Fio` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `secret_code` varchar(255) DEFAULT NULL,
  `anketa` blob,
  PRIMARY KEY (`id_stud`),
  KEY `student_ibfk_1` (`id_group`),
  CONSTRAINT `student_ibfk_1` FOREIGN KEY (`id_group`) REFERENCES `group` (`id_group`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

/*Data for the table `student` */

LOCK TABLES `student` WRITE;

insert  into `student`(`id_stud`,`id_group`,`Fio`,`email`,`secret_code`,`anketa`) values (3,1,'test','ghj',NULL,NULL);
insert  into `student`(`id_stud`,`id_group`,`Fio`,`email`,`secret_code`,`anketa`) values (4,1,'test','ghj',NULL,NULL);
insert  into `student`(`id_stud`,`id_group`,`Fio`,`email`,`secret_code`,`anketa`) values (5,1,'test','ghj','Ih-ns7410H2uK0ECUJRNxyQbiz9UVCJEBxBaeITI2MUhu-Bp7HuVx6mvuHdoySbxHzJiPerT3mLmQsn6eWSbOx-xN147zPuzD5sgobSGlbhKfj8XH_xaJbMT2TRirrB7','{\"email\":{\"name\":\"email\",\"teg\":\"textfield\",\"value\":\"vdgb\",\"required\":false,\"label\":\"Your E-Mail\",\"errors\":[]},\"f1\":{\"name\":\"f1\",\"teg\":\"file\",\"value\":\"kurs1\\/1_1_5_f1.jpg\",\"required\":false,\"label\":\"load file hhjfkldhf\",\"errors\":[]}}');
insert  into `student`(`id_stud`,`id_group`,`Fio`,`email`,`secret_code`,`anketa`) values (6,1,'test','ghj','KvrYVft9DABKu3I9fERyEqnYmpasonvjA1VrJdgeqsthxAqU','{\"email\":{\"name\":\"email\",\"teg\":\"textfield\",\"value\":\"vdgb\",\"required\":false,\"label\":\"Your E-Mail\",\"errors\":[]},\"f1\":{\"name\":\"f1\",\"teg\":\"file\",\"value\":\"kurs1\\/1_1_6_f1.jpg\",\"required\":false,\"label\":\"load file hhjfkldhf\",\"errors\":[]}}');
insert  into `student`(`id_stud`,`id_group`,`Fio`,`email`,`secret_code`,`anketa`) values (7,1,'test','ghj','h9MjO7oI5I4B2FyViepqk7J4igMr1LCaMipPw_JMaJ7Q9QRE','{\"email\":{\"name\":\"email\",\"teg\":\"textfield\",\"value\":\"vdgb\",\"required\":false,\"label\":\"Your E-Mail\",\"errors\":[]},\"f1\":{\"name\":\"f1\",\"teg\":\"file\",\"value\":\"kurs1\\/1_1_7_f1.jpg\",\"required\":false,\"label\":\"load file hhjfkldhf\",\"errors\":[]}}');
insert  into `student`(`id_stud`,`id_group`,`Fio`,`email`,`secret_code`,`anketa`) values (8,1,'test','ghj','XmxbIM70BJ2k5fbLHL6skJbKJlZeBOIoHPva6JHDiR2HOebX','{\"email\":{\"name\":\"email\",\"teg\":\"textfield\",\"value\":\"vdgb\",\"required\":false,\"label\":\"Your E-Mail\",\"errors\":[]},\"f1\":{\"name\":\"f1\",\"teg\":\"file\",\"value\":\"kurs1\\/1_1_8_f1.jpg\",\"required\":false,\"label\":\"load file hhjfkldhf\",\"errors\":[]}}');
insert  into `student`(`id_stud`,`id_group`,`Fio`,`email`,`secret_code`,`anketa`) values (9,1,'test','123123@123.45','GQ7iWjsk9pG7jwTC69dHeqX44zWSL6ABnzGgSHhlqoXabLHM','{\"email\":{\"name\":\"email\",\"teg\":\"textfield\",\"value\":\"rrrrrrr\",\"required\":false,\"label\":\"Your E-Mail\",\"errors\":[]},\"f1\":{\"name\":\"f1\",\"teg\":\"file\",\"value\":\"kurs1\\/1_1_9_f1.jpg\",\"required\":false,\"label\":\"load file hhjfkldhf\",\"errors\":[]}}');
insert  into `student`(`id_stud`,`id_group`,`Fio`,`email`,`secret_code`,`anketa`) values (10,1,'5555','123123@123.45','Bdn6A4a3Pd26HrHvdHnv_qMVPe7sdFKYwDUgkaIv8MxsCItA','{\"email\":{\"name\":\"email\",\"teg\":\"textfield\",\"value\":\"d\'fjklgv\",\"required\":false,\"label\":\"Your E-Mail\",\"errors\":[]},\"f1\":{\"name\":\"f1\",\"teg\":\"file\",\"value\":\"kurs1\\/1_1_10_f1.jpg\",\"required\":false,\"label\":\"load file hhjfkldhf\",\"errors\":[]}}');
insert  into `student`(`id_stud`,`id_group`,`Fio`,`email`,`secret_code`,`anketa`) values (11,1,'5555','123123@123.45','VscgZIQ6YDDyNoapxgQ1ym0T0eSbdl5bgKQmXIeHbOtwMNod','{\"email\":{\"name\":\"email\",\"teg\":\"textfield\",\"value\":\"d\'fjklgv\",\"required\":false,\"label\":\"Your E-Mail\",\"errors\":[]},\"f1\":{\"name\":\"f1\",\"teg\":\"file\",\"value\":\"kurs1\\/1_1_11_f1.jpg\",\"required\":false,\"label\":\"load file hhjfkldhf\",\"errors\":[]}}');

UNLOCK TABLES;

/*Table structure for table `user_kurs` */

CREATE TABLE `user_kurs` (
  `userId` int(11) NOT NULL,
  `id_kurs` bigint(20) NOT NULL,
  PRIMARY KEY (`userId`,`id_kurs`),
  KEY `userId` (`userId`),
  KEY `id_kurs` (`id_kurs`),
  CONSTRAINT `user_kurs_ibfk_2` FOREIGN KEY (`id_kurs`) REFERENCES `kurs` (`id_kurs`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_kurs_ibfk_1` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `user_kurs` */

LOCK TABLES `user_kurs` WRITE;

insert  into `user_kurs`(`userId`,`id_kurs`) values (1,1);
insert  into `user_kurs`(`userId`,`id_kurs`) values (11,1);

UNLOCK TABLES;

/*Table structure for table `users` */

CREATE TABLE `users` (
  `userId` int(11) NOT NULL AUTO_INCREMENT,
  `userLogin` varchar(255) DEFAULT NULL COMMENT 'Логін користувача',
  `userPassword` varchar(255) DEFAULT NULL COMMENT 'Пароль',
  `userPIB` varchar(255) DEFAULT NULL COMMENT 'ПІБ',
  `userContactData` text COMMENT 'Контактна інформація',
  `userLastLogin` datetime DEFAULT NULL COMMENT 'Last login datetime',
  `userIsActive` tinyint(4) DEFAULT NULL COMMENT 'If user is allowed to login',
  `userToken` varchar(255) DEFAULT NULL,
  `userEmail` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`userId`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COMMENT='System users';

/*Data for the table `users` */

LOCK TABLES `users` WRITE;

insert  into `users`(`userId`,`userLogin`,`userPassword`,`userPIB`,`userContactData`,`userLastLogin`,`userIsActive`,`userToken`,`userEmail`) values (1,'admin','jKkTCfzjlNNsk',NULL,NULL,NULL,NULL,NULL,NULL);
insert  into `users`(`userId`,`userLogin`,`userPassword`,`userPIB`,`userContactData`,`userLastLogin`,`userIsActive`,`userToken`,`userEmail`) values (11,'op1',NULL,'op1','nv bv v mvb',NULL,1,NULL,'op1@test.server');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
