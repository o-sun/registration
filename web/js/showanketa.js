window.anketa = window.anketa || {};

anketa.dataCorrect = {};
anketa.fn = {};

anketa.fn.requiredValidator = function (event) {
    var self = $(event.target);
    var value = self.val().trim();
    if (value && value.length > 0) {
        anketa.dataCorrect[self.attr('name')] = true;
        self.next('.message').html('');
    } else {
        anketa.dataCorrect[self.attr('name')] = false;
        self.next('.message').html('<span class="error">Заповніть це поле</span>');
    }
};

anketa.fn.requiredSelectValidator = function (event) {
    var self = $(event.target);
    var value = self.val();
    if (value && value.length > 0) {
        anketa.dataCorrect[self.attr('name')] = true;
        self.siblings('.message').html('');
    } else {
        anketa.dataCorrect[self.attr('name')] = false;
        self.siblings('.message').html('<span class="error">Заповніть це поле</span>');
    }
};

anketa.fn.emailValidator = function (event) {
    var self = $(event.target);
    var value = self.val().trim();
    if (value && value.length > 0) {
        
        var expr = /@([0-9a-z_-]+\.)+([0-9a-z_-]+)$/i;
        if(expr.test(value)){
            self.next('.message').html('');
            anketa.dataCorrect[self.attr('name')] = true;
        }else{
            anketa.dataCorrect[self.attr('name')] = false;
            self.next('.message').html('<span class="error">Впишіть адресу електронної пошти</span>');
        }
    }
};

anketa.fn.requiredRadioValidator=function (event) {
    var self = $(event.target);
    var name = self.attr('name');
    // var selectedVal = "";
    var selected = $("input[type='radio'][name='"+name+"']:checked");
    if (selected.length > 0) {
        // selectedVal = selected.val();
        anketa.dataCorrect[name] = true;
        self.parent().siblings('.message').html('');
    }else{
        anketa.dataCorrect[name] = false;
        self.parent().siblings('.message').html('<span class="error">Заповніть це поле</span>');
    }
};

anketa.fn.requiredCheckboxValidator=function (event) {
    var self = $(event.target);
    var name = self.attr('name');
    // var selectedVal = "";
    var selected = $("input[type='checkbox'][name='"+name+"']:checked");
    if (selected.length > 0) {
        // selectedVal = selected.val();
        anketa.dataCorrect[name] = true;
        self.parent().siblings('.message').html('');
    }else{
        anketa.dataCorrect[name] = false;
        self.parent().siblings('.message').html('<span class="error">Ви повинні поставити цю відмітку</span>');
    }
};

anketa.fn.formatDateValidator=function(format)  {
    
    var regexp=new RegExp("^"+format.replace(/[dmy]/gi,"[0-9]")+"$");
    
    return function(event){
        var self = $(event.target);
        var value = self.val().trim();
        // console.log(regexp);
        if (value && value.length > 0) {
            if(regexp.test(value)){
                self.next('.message').html('');
                anketa.dataCorrect[self.attr('name')] = true;
            }else{
                anketa.dataCorrect[self.attr('name')] = false;
                self.next('.message').html('<span class="error">Впишіть дату у форматі '+format+'</span>');
            }
        }
    };
};

anketa.fn.validate=function(event){
    anketa.dataCorrect = {};
    $('.validable').trigger('change');
    var allDataCorrect=true;
    for(var key in anketa.dataCorrect){
        if(!anketa.dataCorrect[key]){
            allDataCorrect=false;
        }
    }
    return allDataCorrect;
};


anketa.fn.resetFileValidator = function (event) {
    var self = $(event.target);
    var name= self.attr('name');
    delete(anketa.dataCorrect[name]);
    //console.log("anketa.fn.resetFileValidator",name, anketa.dataCorrect[name]);
};

anketa.fn.requiredFileValidator = function (event) {
    var self = $(event.target);
    var name= self.attr('name');
    //console.log("anketa.fn.requiredFileValidator",name);
    
    if (typeof FileReader === "undefined"){
        return;
    }
    
    if(typeof anketa.dataCorrect[name] !== "undefined" && anketa.dataCorrect[name]===false){
        return;
    }

    anketa.dataCorrect[name] = false;

    if (typeof FileReader !== "undefined") {
        //var size = document.getElementById('myfile').files[0].size;
        try {
            var size = self.context.files[0].size;
            anketa.dataCorrect[name] = (size > 0);
        } catch (err) {
        }
    }
    if (!anketa.dataCorrect[name]) {
        var oldFile = $('input[name="' + self.attr('data-prev') + '"]').val();
        if (oldFile) {
            anketa.dataCorrect[name] = true;
        }
    }
    //console.log("anketa.fn.resetFileValidator",name, anketa.dataCorrect[name]);
    if (anketa.dataCorrect[name]) {
        self.siblings('.message').html('');
    } else {
        self.siblings('.message').html('<span class="error">Заповніть це поле</span>');
    }
};

anketa.fn.fileSizeValidator = function (event) {
    var self = $(event.target);
    var name = self.attr('name');
    //console.log("anketa.fn.fileSizeValidator",name);

    if(typeof anketa.dataCorrect[name] !== "undefined" && anketa.dataCorrect[name]===false){
        return;
    }

    var maxSize=parseInt(self.attr('data-max-size'));
    if(isNaN(maxSize)){
        return;
    }
    if (typeof FileReader !== "undefined") {
        try {
            var size = self.context.files[0].size;
            anketa.dataCorrect[name] = (size <= maxSize);
            //console.log("anketa.fn.fileSizeValidator",name, anketa.dataCorrect[name]);
            if (anketa.dataCorrect[name]){
                self.siblings('.message').html('');
            } else {
                self.siblings('.message').html('<span class="error">Виберіть файл меншого розміру</span>');
            }
        } catch (err) {
        }
    }
};

anketa.fn.fileExtensionValidator = function (event) {
    var self = $(event.target);
    var name = self.attr('name');
    // console.log("anketa.fn.fileExtensionValidator",name);
    if (typeof FileReader === "undefined") {
       return;
    }
    if(typeof anketa.dataCorrect[name] !== "undefined" && anketa.dataCorrect[name]===false){
        return;
    }
    try{
        var regexp=new RegExp("\\.("+self.attr('data-extension')+")$","i");
        var filename = ''+self.context.files[0].name;
        anketa.dataCorrect[name] = regexp.test(filename.toLowerCase());
        // console.log("anketa.fn.fileExtensionValidator",name, filename, regexp, anketa.dataCorrect[name]);
        if (anketa.dataCorrect[name]){
            self.siblings('.message').html('');
        } else {
            self.siblings('.message').html('<span class="error">Виберіть файл із розширенням '+self.attr('data-extension')+'</span>');
        }
    }catch(error){
        
    }
};



anketa.fn.numberValidator=function (event) {
    var self = $(event.target);
    var value = self.val().trim();
    if (value && value.length > 0) {
        if(isNaN(parseFloat(value))){
            anketa.dataCorrect[self.attr('name')] = false;
            self.next('.message').html('<span class="error">Впишіть число</span>');
        }else{
            self.next('.message').html('');
            anketa.dataCorrect[self.attr('name')] = true;
        }
    }
};


anketa.fn.integerValidator=function (event) {
    var self = $(event.target);
    var value = self.val().trim();
    if (value && value.length > 0) {
        if(isNaN(parseInt(value))){
            anketa.dataCorrect[self.attr('name')] = false;
            self.next('.message').html('<span class="error">Впишіть ціле число</span>');
        }else{
            self.next('.message').html('');
            anketa.dataCorrect[self.attr('name')] = true;
        }
    }
};



/*
  {
    "teg":"tree",
    "name":"department",
    "label":"Факультет и специальность",
    "required":true,
    "value":"biolog.chem"
    "items":[
       {
          "key":"biolog",
          "value":"Білогічний",
          "items":[
              {
                 "key":"chem",
                 "value":"Хімія",
              },
              {
                 "key":"biol",
                 "value":"Біологія",
              },
              {
                 "key":"eco",
                 "value":"Екологія",
              }
          ]
       },
       {
          "key":"",
          "value":"",
          "items":[
          ]
       }
    ]
  }


 */


//{
//    containerId:\"tree{$arg['name']}\",
//    name:\"{$this->formName}[{$arg['name']}]\",
//    reuired: true | false
//    value:\"{$arg['value']}\",
//        "items":{
//          "biolog":{"label":"Біологічний факультет","value":"",
//              "items":{
//                "chem":{"label":"Хімія"},
//                "biol":{"label":"Біологія"},
//                "ecol":{"label":"Екологія, охорона навколишнього середовища та збалансоване природокористування"}
//              }
//           },
//          "econom":{"label":"Економічний факультет"},
//          "fif":{"label":"Факультет іноземної філології"},
//          "sport":{"label":"Факультет фізичного виховання"},
//          "history":{"label":"Історичний факультет"},
//          "journalist":{"label":"Факультет журналістики"},
//          "law":{"label":"Юридичний факультет"},
//          "manager":{"label":"Факультет менеджменту"},
//          "mathem":{"label":"Математичний факультет"},
//          "phylology":{"label":"Філологічний факультет"},
//          "physics":{"label":"Фізичний факультет"},
//          "spp":{"label":"Факультет соціальної педагогіки та психології"},
//          "sociolog":{"label":"Факультет соцiологiї та управлiння"},
//          "koledg-epk":{"label":"Економіко-правничий коледж"},
//          "koledg-torg":{"label":"Торговий коледж"}
//       }       
//    }
//
anketa.tree=anketa.tree || {};
anketa.fn.tree = function(options){

    options.init=true;
    anketa.fn.treeNodeActivate(options, options);
    
    options.hiddenFormElement=$("<input>",{type:"hidden", name:options.name, class:(options.required?'tree required validable':'tree')});
    
    $(options.containerId)
            .empty().append(options.element)
            .append(options.hiddenFormElement)
    ;

    
    options.updateValue=function(){
        var value = options.getValue();
        // alert(value);
        options.hiddenFormElement.val(value);

        $(options.containerId).next('.message').html('');

        if(!options.init && options.required){
            var pattern = /[^/]$/;
            if(value && value.length > 0 && pattern.test(value)){
                anketa.dataCorrect[options.name] = true;
            }else{
                anketa.dataCorrect[options.name] = false;
            }
            if(!anketa.dataCorrect[options.name]){
                $(options.containerId).next('.message').html('<span class="error">Виберіть значення</span>');
            }
        }
    }
    options.valueChangeTimeout=false;
    options.onValueChanged=function(){
        if(options.valueChangeTimeout){
            clearTimeout(options.valueChangeTimeout);
        }
        options.valueChangeTimeout = setTimeout(options.updateValue, 500);
    };
    options.setValue(options.value);
    options.updateValue();

    options.init=false;

};



anketa.fn.treeNodeGetValue=function(){
    return this.value + 
          (  ( this.items && this.items[this.value]  && this.items[this.value].items) 
             ? ('/'+this.items[this.value].getValue())
             : '' );
};
anketa.fn.treeNodeSetValue=function(val){

    this.subselector.hide().empty();
    
    var nextVal;
    if( Object.prototype.toString.call( val ) === '[object Array]' ) {
        nextVal=JSON.parse(JSON.stringify(val));
    }else{
        nextVal=(''+val).split(/ *\/ */);
    }
    if(nextVal.length>0
       && this.items
       && Object.prototype.toString.call( this.items ) === '[object Object]'
      ){
        if(this.items[nextVal[0]]){
            this.value = nextVal.shift();
            this.selector.val(this.value);
            if(this.items[this.value].items){
                this.items[this.value].setValue(nextVal);
                this.items[this.value].selector.change(this.items[this.value].selectorChanged);
                this.subselector.show().append(this.items[this.value].element); //
            }
        }else{
            this.value = "";
        }
        // this.root.onValueChanged();
    };
};

// extend node with methods
anketa.fn.treeNodeActivate=function(node, root){
    
    node.getValue=anketa.fn.treeNodeGetValue;
    node.setValue=anketa.fn.treeNodeSetValue;
    node.root=root;
    // add hidden form element after container
    node.element=$('<div/>',{class:"tree-level-selector"});
    
    node.selector=$('<select></select>',{class:"form-control"});
    node.selectorChanged=function(){ 
        // var newValue=node.selector.val();
        var newValue = node.selector.find(":selected").attr('value');
        // alert(newValue);
        node.setValue([newValue]);  
        node.root.onValueChanged();
    };
    node.selector.change(node.selectorChanged);
    // console.log('node.items ',Object.prototype.toString.call( node.items ));
    if( node.items && Object.prototype.toString.call( node.items ) === '[object Object]'){
        node.selector.append($('<option>',{value:''}).html(''));
        for(var key in node.items){
            node.selector.append($('<option>',{value:key}).html(node.items[key].label));
        }
    };
    node.element.append(node.selector);
    
    node.subselector=$('<div/>',{class:"tree-level-selector"});
    node.element.append(node.subselector);


    if( node.items && Object.prototype.toString.call( node.items ) === '[object Object]'){
        for(var h in node.items){
            anketa.fn.treeNodeActivate(node.items[h], root)
        }
    };
    // console.log(node);
};


//extension

$(document).ready(function () {
    $("select.anketa-select").select2();

    $('.anketa-textfield.required').change(anketa.fn.requiredValidator).addClass('validable');
    $('.anketa-textfield.email').change(anketa.fn.emailValidator).addClass('validable');
    $('.anketa-textfield.number').change(anketa.fn.numberValidator).addClass('validable');
    $('.anketa-textfield.integer').change(anketa.fn.integerValidator).addClass('validable');

    $('.anketa-textarea.required').change(anketa.fn.requiredValidator).addClass('validable');

    $('.anketa-select.required').change(anketa.fn.requiredSelectValidator).addClass('validable');
    $('.anketa-radio.required').change(anketa.fn.requiredRadioValidator).addClass('validable');
    $('.anketa-checkbox.required').change(anketa.fn.requiredCheckboxValidator).addClass('validable');


    $('.anketa-file.required').change(anketa.fn.resetFileValidator);
    $('.anketa-file[data-max-size]').change(anketa.fn.resetFileValidator);
    $('.anketa-file[data-extension]').change(anketa.fn.resetFileValidator);

    $('.anketa-file.required').change(anketa.fn.requiredFileValidator).addClass('validable');
    $('.anketa-file[data-max-size]').change(anketa.fn.fileSizeValidator).addClass('validable');
    $('.anketa-file[data-extension]').change(anketa.fn.fileExtensionValidator).addClass('validable');

    // twitter bootstrap datepicker
    $('.anketa-date').datepicker({ format: 'dd.mm.yyyy', language:'uk'});
    $('.anketa-date').change(anketa.fn.formatDateValidator('dd.mm.yyyy')).addClass('validable');
    
    
    if(anketa.tree){
        for(var i in anketa.tree){
            //console.log(i,anketa.tree[i]);
            anketa.fn.tree(anketa.tree[i]);
        }
    }
});
