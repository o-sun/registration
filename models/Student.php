<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "student".
 *
 * @property string $id_stud
 * @property string $id_group
 * @property string $Fio
 * @property string $email
 * @property string $secret_code
 * @property resource $anketa
 *
 * @property Group $idGroup
 */
class Student extends \yii\db\ActiveRecord
{
    
    public static function getStatusOptions(){
        return [
            ''=>'',
            'new'=>Yii::$app->lang->t('status_new'),
            'rejected'=>Yii::$app->lang->t('status_rejected'),
            'accepted'=>Yii::$app->lang->t('status_accepted')
        ];
    } 
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'student';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_group'], 'integer'],
            [['anketa','keyWords','status'], 'string'],
            [['firstName','lastName', 'email'], 'required'],
            [['firstName','lastName','middleName', 'email', 'secret_code'], 'string', 'max' => 255],
            ['email', 'email'],
            [['dateCreated'], 'date', 'format' => 'php:Y-m-d']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_stud' => Yii::$app->lang->t('id_stud'),
            'id_group' => Yii::$app->lang->t('id_group'),
            'firstName' => Yii::$app->lang->t('firstName'),
            'middleName' => Yii::$app->lang->t('middleName'),
            'lastName' => Yii::$app->lang->t('lastName'),
            'email' => Yii::$app->lang->t('email'),
            'secret_code' => Yii::$app->lang->t('secret_code'),
            'anketa' => Yii::$app->lang->t('anketa'),
            'dateCreated'=>Yii::$app->lang->t('dateCreated'),
            'keyWords'=>Yii::$app->lang->t('keyWords'),
            'status'=>Yii::$app->lang->t('status'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdGroup()
    {
        return $this->hasOne(Group::className(), ['id_group' => 'id_group']);
    }
    
    public function getKeyWords(){
        $kw='';

        $group = $this->idGroup;
        $kurs = $group->idKurs;

        $kw.="
            ".Yii::$app->lang->t('Course')." {$kurs->name_kurs}
            ".Yii::$app->lang->t('Group')." {$group->name_group}
            ".Yii::$app->lang->t('firstName')." {$this->firstName}
            ".Yii::$app->lang->t('middleName')." {$this->middleName}
            ".Yii::$app->lang->t('lastName')." {$this->lastName}
            ".Yii::$app->lang->t('email')." {$this->email}
        ";
            
        $obj = new \app\lib\ShowAnketa('anketa');
        $anketaData = json_decode($this->anketa,true);
        if($anketaData){
            $post=['anketa'=> array_map(function($x){return $x['value'];}, $anketaData )];
        }else{
            $post=['anketa'=> []];
        }
        $anketa = $group->blankAnkety();
        $kw.=strip_tags(str_replace('<',' <',$obj->view($anketa, $post)));
        
        $kw = preg_replace("/( |\\t|\n)+/",' ',mb_strtolower($kw,"UTF-8"));
        $kw = preg_replace("/( |\\t|\n)+/",' ',$kw);
        // var_dump($kw); exit();
        return $kw;
    }
}
