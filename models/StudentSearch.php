<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Student;

/**
 * StudentSearch represents the model behind the search form about `app\models\Student`.
 */
class StudentSearch extends Student {

    // public $keyWords;
    
    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_stud', 'id_group'], 'integer'],
            [['dateCreated'], 'date', 'format' => 'php:Y-m-d'],
            [['firstName','middleName','lastName', 'email', 'secret_code', 'anketa','keyWords','status'], 'safe'],
        ];
    }
    
    //    /**
    //     * @inheritdoc
    //     */
    //    public function attributeLabels()
    //    {
    //        $p=parent::attributeLabels();
    //        $p['keyWords']=Yii::$app->lang->t('keyWords');
    //        return $p;
    //    }
    
    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = Student::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_stud' => $this->id_stud,
            'id_group' => $this->id_group,
        ]);

        $query->andFilterWhere(['like', 'firstName', $this->firstName])
                ->andFilterWhere(['like', 'middleName', $this->firstName])
                ->andFilterWhere(['like', 'lastName', $this->firstName])
                ->andFilterWhere(['like', 'email', $this->email])
                ->andFilterWhere(['status' => $this->status])
                ->andFilterWhere(['like', 'secret_code', $this->secret_code])
                ->andFilterWhere(['dateCreated' => $this->dateCreated])
                ->andFilterWhere(['like', 'anketa', $this->anketa]);
        
        
        $kw=preg_split("/[- ,;!?\"')(}{]/",trim($this->keyWords));

        foreach($kw as $word){
            if(strlen($word)>0){
                $query->andFilterWhere(['like', 'keyWords', mb_strtolower($word,"UTF-8")]);
            }
        }
        

        return $dataProvider;
    }

}
