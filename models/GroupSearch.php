<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Group;
use yii\db\Query;

/**
 * GroupSearch represents the model behind the search form about `app\models\Group`.
 */
class GroupSearch extends Group {

    public $count_stud;

    public function __construct($id_kurs) {
        $this->id_kurs = $id_kurs;
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['id_group', 'id_kurs', 'max_count_stud', 'count_stud'], 'integer'],
            [['name_group', 'date_start', 'anketa'], 'safe'],
            [['visible'], 'boolean'],
        ];
    }

    public function attributeLabels() {
        $lbl = parent::attributeLabels();
        $lbl['count_stud'] = Yii::$app->lang->t('NumOfMembers');
        return $lbl;
    }

    /**
     * @inheritdoc
     */
    public function scenarios() {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        //$query = Group::find();
        $query = new Query();
        $query->from('group')
                ->leftJoin('student', 'student.id_group = group.id_group')
                ->innerJoin('kurs', 'kurs.id_kurs = group.id_kurs')
                ->select('group.id_group,
                        group.id_kurs,
                        group.name_group,
                        group.date_start,
                        group.max_count_stud,
                        group.visible,
                        kurs.name_kurs,
                        count(student.id_stud) as count_stud
                        ')
                ->groupBy('`group`.id_group');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere(['group.id_kurs' => $this->id_kurs]);



        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'group.id_group' => $this->id_group,
            'group.id_kurs' => $this->id_kurs,
            'group.date_start' => $this->date_start,
            'group.max_count_stud' => $this->max_count_stud,
            'group.visible' => $this->visible,
        ]);

        $query->andFilterWhere(['like', 'group.name_group', $this->name_group])
                ->andFilterWhere(['like', 'group.anketa', $this->anketa]);


        return $dataProvider;
    }

}
