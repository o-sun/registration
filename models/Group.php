<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "group".
 *
 * @property string $id_group
 * @property string $id_kurs
 * @property string $name_group
 * @property string $date_start
 * @property integer $max_count_stud
 * @property boolean $visible
 * @property resource $anketa
 *
 * @property Kurs $idKurs
 * @property Student[] $students
 */
class Group extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kurs', 'name_group', 'date_start'], 'required'],
            [['id_kurs', 'max_count_stud'], 'integer'],
            [['date_start'], 'safe'],
            [['visible'], 'boolean'],
            [['anketa'], 'string'],
            [['name_group'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_group' => Yii::$app->lang->t('id_group'),
            'id_kurs' => Yii::$app->lang->t('id_kurs'),
            'name_group' => Yii::$app->lang->t('name_group'),
            'date_start' => Yii::$app->lang->t('date_start'),
            'max_count_stud' => Yii::$app->lang->t('max_count_stud'),
            'visible' => Yii::$app->lang->t('visible'),
            'anketa' => Yii::$app->lang->t('anketa'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdKurs()
    {
        return $this->hasOne(Kurs::className(), ['id_kurs' => 'id_kurs']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudents()
    {
        return $this->hasMany(Student::className(), ['id_group' => 'id_group']);
    }
    
    
    public function blankAnkety(){
        $blank = trim($this->anketa);
        if(strlen($blank) ==0){
            $kurs=$this->idKurs;
            $blank=$kurs->anketa;
        }
        return json_decode($blank, true);
    }
    
    public function isExpired(){
        $timestamp=strtotime($this->date_start);
        return (time() - 24*3600 ) > $timestamp;
    }
}
