<?php

namespace app\models;

use Yii;
use app\lib\ValidatorDatetime;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface {

    use ValidatorDatetime;

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'users';
    }
    
    public function behaviors() {
        return [
            [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'idKurs' => 'kurs'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['userId', 'userIsActive'], 'integer'],
            [['userLogin', 'userPassword', 'userPIB', 'userToken', 'userEmail'], 'string', 'max' => 255],
            [['userContactData'], 'string'],
            [['userLastLogin'], 'default', 'value' => null],
            [['userLastLogin'], 'datetime'],
            [['userLogin'], 'unique'],
            [['idKurs'], 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'userId' => Yii::$app->lang->t('userId'),
            'userLogin' => Yii::$app->lang->t('userLogin'),
            'userPassword' => Yii::$app->lang->t('userPassword'),
            'userPIB' => Yii::$app->lang->t('userPIB'),
            'userContactData' => Yii::$app->lang->t('userContactData'),
            'userLastLogin' => Yii::$app->lang->t('userLastLogin'),
            'userIsActive' => Yii::$app->lang->t('userIsActive'),
            'userEmail' => 'Email',
            'userToken' => 'userToken',
            'idKurs'=>Yii::$app->lang->t('idKurs')
        ];
    }
    
     public function getKurs() {
        return $this->hasMany(Kurs::className(), ['id_kurs' => 'id_kurs'])
                        ->viaTable('user_kurs', ['userId' => 'userId']);
    }

    public function getAuthKey() {
        return $this->userToken;
    }

    public function getId() {
        return $this->userId;
    }

    public function validateAuthKey($authKey) {
        return $this->userToken == $authKey;
    }

    public static function findIdentity($id) {
        return static::findOne($id);
    }

    public static function findIdentityByAccessToken($token, $type = null) {
        return static::findOne(["userToken" => $token]);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey() {
        $this->userToken = Yii::$app->security->generateRandomString(32);
    }

    public function setPassword($newPassword) {
        $this->userPassword = crypt($newPassword, Yii::$app->params['salt']);
    }

    /**
     * Finds by the username
     */
    public static function findByUsername($username) {
        return static::findOne(["userLogin" => $username]);
    }

    public function validatePassword($password) {
        $encodedPassword = crypt($password, Yii::$app->params['salt']);
        return $encodedPassword == $this->userPassword;
    }

    public function validateUserStatusActive() {
        return $this->userIsActive;
    }

    public function asArray() {
        return [
            'userId' => $this->userId,
            'userLogin' => $this->userLogin,
            'userPassword' => '****',
            'userPIB' => $this->userPIB,
            'userContactData' => $this->userContactData,
            'userLastLogin' => $this->userLastLogin,
            'userIsActive' => $this->userIsActive,
            'userEmail' => $this->userEmail,
            'userToken' => ''
        ];
    }

    //    if(\app\models\User::isAdmin()){
    //        
    //    }
    public static function isAdmin() {
        if (Yii::$app->user->isGuest) {
            return false;
        }
        return in_array(Yii::$app->user->identity->userLogin, Yii::$app->params['admins']);
    }

}
