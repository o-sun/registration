<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kurs".
 *
 * @property string $id_kurs
 * @property string $name_kurs
 * @property string $description
 * @property resource $anketa
 * @property boolean $visible
 *
 * @property Group[] $groups
 */
class Kurs extends \yii\db\ActiveRecord {

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'kurs';
    }

    public function behaviors() {
        return [
            [
                'class' => \voskobovich\behaviors\ManyToManyBehavior::className(),
                'relations' => [
                    'userIds' => 'users'
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['name_kurs'], 'required'],
            [['description', 'anketa'], 'string'],
            [['visible'], 'boolean'],
            [['name_kurs'], 'string', 'max' => 255],
            [['userIds'], 'each', 'rule' => ['integer']]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id_kurs' => Yii::$app->lang->t('id_kurs'),
            'name_kurs' => Yii::$app->lang->t('name_kurs'),
            'description' => Yii::$app->lang->t('description'),
            'anketa' => Yii::$app->lang->t('anketa'),
            'visible' => Yii::$app->lang->t('visible'),
            'userIds'=>Yii::$app->lang->t('userIds')
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroups() {
        return $this->hasMany(Group::className(), ['id_kurs' => 'id_kurs']);
    }
    public function getUsers() {
        return $this->hasMany(User::className(), ['userId' => 'userId'])
                        ->viaTable('user_kurs', ['id_kurs' => 'id_kurs']);
    }

    public function isKursManager(){
        if(Yii::$app->user->isGuest){
            return false;
        }
        if(User::isAdmin()){
            return true;
        }
        // $query=$this->getUsers()->andWhere(['userId'=>Yii::$app->user->identity->userId, 'id_kurs'=>$this->id_kurs]);
        $query=$this->getUsers()->andWhere(['userId'=>Yii::$app->user->identity->userId])->all();
        // var_dump($query);
        // $list=$query->one();
        // exit();
        return count($query)>0;
    }
}
