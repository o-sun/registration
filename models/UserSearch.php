<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User;

/**
 * UserSearch represents the model behind the search form about `app\models\User`.
 */
class UserSearch extends User
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['userId', 'userIsActive'], 'integer'],
            [['userLogin', 'userPassword', 'userPIB', 'userContactData', 'userLastLogin', 'userToken', 'userEmail'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = User::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'userId' => $this->userId,
            'userLastLogin' => $this->userLastLogin,
            'userIsActive' => $this->userIsActive,
        ]);

        $query->andFilterWhere(['like', 'userLogin', $this->userLogin])
            ->andFilterWhere(['like', 'userPassword', $this->userPassword])
            ->andFilterWhere(['like', 'userPIB', $this->userPIB])
            ->andFilterWhere(['like', 'userContactData', $this->userContactData])
            ->andFilterWhere(['like', 'userToken', $this->userToken])
            ->andFilterWhere(['like', 'userEmail', $this->userEmail]);

        return $dataProvider;
    }
}
