<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Kurs;

/**
 * KursSearch represents the model behind the search form about `app\models\Kurs`.
 */
class KursSearch extends Kurs
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kurs'], 'integer'],
            [['name_kurs', 'description', 'anketa'], 'safe'],
            [['visible'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kurs::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id_kurs' => $this->id_kurs,
            'visible' => $this->visible,
        ]);

        $query->andFilterWhere(['like', 'name_kurs', $this->name_kurs])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'anketa', $this->anketa]);

        return $dataProvider;
    }
}
